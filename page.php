<?php get_header(); ?>

	<?php get_template_part('template-parts/content', 'pagehead'); ?>
	
	<div class="wrap <?php the_field('sidebar_position'); ?>">
		
		<?php 
			if (is_page(array(21))) { 
				get_sidebar();
			} 
		?>
		
		<article id="content">
			
			<?php 
				if (!is_front_page()) { 
					$type = get_field('page_title');
					switch ($type) {
						case 'default':
							echo '<h1 class="page-title">'.get_the_title().'</h1>';
						break;
						case 'custom':
							echo '<h1 class="page-title">'.get_field('custom_page_title').'</h1>';
						break;
					}
				} 
			?>
			
			
			<?php
				if ( have_posts() ) { while ( have_posts() ) { the_post();
					the_content();
				} }
			?>
			
		</article>
		
		<?php 
			if ( (get_field('sidebar_position') != 'full-width' ) ) { 
				get_sidebar();
			}
		?>
		
	</div>
	
<?php get_footer(); ?>