<?php get_header(); ?>

    <div class="wrap full-width">
        <div id="content">
            <?php
                if (have_posts()) : while (have_posts()) : the_post();
                
                    get_template_part( 'template-parts/content', 'excerpt' );
                    
                endwhile;
                
                    get_template_part('template-parts/post', 'pagination');  

                else :
                
                    get_template_part( 'template-parts/content', 'none' );
                    
                endif;
            ?>
        </div>
    </div>

<?php get_footer(); ?>