<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="referrer" content="origin">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

	<header class="header <?php echo get_theme_mod('bir_header_type'); ?>" id="top">
		<div class="wrap">
			
			<div id="branding"><?php the_custom_logo(); ?></div>
			
			<?php if ( has_nav_menu( 'primary' ) ) { ?>
				<div id="nav-expander" class="nav-expander">
					<span class="burger-icon"></span>
				</div>

				<nav class="sitenav">
					
					<a id="homelink" href="<?php bloginfo('url'); ?>">
						<svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="196" height="22.545"><path d="M6.856 20.941L.163 2.7A1.994 1.994 0 010 1.95 1.964 1.964 0 012.036.063a2.021 2.021 0 011.973 1.415L9.216 16.6l5.109-15.061A2.088 2.088 0 0116.33 0h.292a2.064 2.064 0 012.005 1.539L23.734 16.6l5.208-15.154A2.02 2.02 0 0130.879.062a1.922 1.922 0 011.975 1.854 2.192 2.192 0 01-.194.787l-6.693 18.238a2.21 2.21 0 01-2.071 1.6h-.387a2.131 2.131 0 01-2.038-1.6l-5.046-14.37-5.043 14.37a2.168 2.168 0 01-2.07 1.6h-.389a2.2 2.2 0 01-2.068-1.6zm29.263-.47V1.98a1.99 1.99 0 013.978 0v7.389H50.9V1.98a1.99 1.99 0 013.978 0v18.49a1.991 1.991 0 01-3.979 0v-7.516H40.1v7.516a1.989 1.989 0 01-3.976 0zm23.833 0V1.98a1.989 1.989 0 013.975 0v18.49a1.989 1.989 0 01-3.975 0zm14.615 0V3.8h-5.5a1.816 1.816 0 01-1.842-1.792A1.817 1.817 0 0169.069.22h15.037a1.808 1.808 0 011.809 1.793A1.807 1.807 0 0184.106 3.8h-5.53v16.67a2.007 2.007 0 01-4.009 0zm14.422-.19V2.17A1.949 1.949 0 0190.994.22h8.148a8.314 8.314 0 015.917 1.95 4.852 4.852 0 011.519 3.646v.064a5.236 5.236 0 01-3.233 4.9c2.652.881 4.5 2.359 4.5 5.346v.063c0 3.93-3.33 6.038-8.376 6.038h-8.47a1.949 1.949 0 01-2.006-1.95zM92.9 9.5h5.433c2.555 0 4.268-.974 4.268-2.987v-.067c0-1.73-1.421-2.8-3.977-2.8H92.9V9.5zm0 9.307h6.6c2.717 0 4.365-1.039 4.365-3.018v-.063c0-1.856-1.518-2.956-4.655-2.956H92.9v6.03zm18.463 1.667V2.17A1.949 1.949 0 01113.37.22h8.085a9.271 9.271 0 016.531 2.2 6.551 6.551 0 011.876 4.747v.063c0 3.52-2.1 5.66-5.141 6.572l4.332 5.315a2.2 2.2 0 01.647 1.483 1.852 1.852 0 01-1.907 1.791 2.29 2.29 0 01-1.94-1.006l-5.5-6.822h-5.012v5.91a1.99 1.99 0 01-3.978 0zm3.978-9.338h5.821c2.845 0 4.656-1.447 4.656-3.68v-.068c0-2.358-1.746-3.648-4.689-3.648h-5.788zm20.169 11.1a1.949 1.949 0 01-2.007-1.95V2.17A1.948 1.948 0 01135.51.22h13.16a1.769 1.769 0 011.778 1.73 1.749 1.749 0 01-1.778 1.729h-11.188V9.4h9.732a1.776 1.776 0 011.779 1.76 1.744 1.744 0 01-1.779 1.7h-9.731v5.911h9.732A1.771 1.771 0 01149 20.5a1.751 1.751 0 01-1.78 1.73h-11.71zm40.021-1.95V2.17a1.947 1.947 0 012-1.95h6.437C191.086.22 196 4.968 196 11.161v.062c0 6.2-4.914 11.007-12.028 11.007h-6.437a1.947 1.947 0 01-2-1.95zm3.977-1.572h4.464c4.753 0 7.857-3.113 7.857-7.421v-.065c0-4.307-3.1-7.482-7.857-7.482h-4.464v14.962zm-11.284 1.814v-3.612h-11.132l-2.23 4.444a1.983 1.983 0 01-2.636.882 1.889 1.889 0 01-.939-2.577l7.487-14.925c.308-.69 2.08-4.521 7.931-4.521L170.2.2a1.959 1.959 0 011.984 1.639l.02.31v18.373a1.989 1.989 0 01-3.976 0zm0-16.77l-1.2-.013-.461.013c-1.913.094-3.258.774-3.864 1.978l-3.891 7.758h9.414V3.753z" fill="#fff" fill-rule="evenodd"/></svg>
					</a>
					
					<ul class="menu">
						
						<?php
		                    $locations = get_nav_menu_locations();
							$css_class = $menu_item->classes;

		                    if ( isset( $locations[ 'primary' ] ) ) {

								$menu = get_term( $locations[ 'primary' ], 'nav_menu' );

								if ( $items = wp_get_nav_menu_items( $menu->name ) ) {
		                            foreach ( $items as $item ) {
		                                echo '<li class="menu-item-'.$item->ID.$css_class.'">';
		                                    echo "<a href=\"{$item->url}\">{$item->title}</a>";

											if ( is_active_sidebar( 'mega-menu-widget-area-' . $item->ID ) ) {
												echo '<span class="sub-toggle"></span>';
												echo "<div class=\"sub-menu\">";
													echo "<ul id=\"mega-menu-{$item->ID}\" class=\"wrap sub-mega-menu\">";
														dynamic_sidebar( 'mega-menu-widget-area-' . $item->ID );
													echo "</ul>";
												echo "</div>";
											}
		                                echo "</li>";
		                            }
		                        }
		                    }
		                ?>
					
						<li class="search-site menu-item-has-children">
							<a href="javascript:;">
								<span class="screen-reader-text">search</span>
								<svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="16" height="16"><path d="M2 7a4.951 4.951 0 015-5 4.951 4.951 0 015 5 4.951 4.951 0 01-5 5 4.951 4.951 0 01-5-5zm12.3 8.7a.99.99 0 001.4-1.4l-3.1-3.1A6.847 6.847 0 0014 7a6.957 6.957 0 00-7-7 6.957 6.957 0 00-7 7 6.957 6.957 0 007 7 6.847 6.847 0 004.2-1.4z" fill="#002d72"/></svg>
							</a>
							
							<div class="sub-menu" id="sp-search">
								<ul class="wrap sub-mega-menu">
									<li>
										<h5>Search</h5>
										<?php get_search_form(); ?>
									</li>
								</ul>
							</div>

						</li>
						
					</ul>
				</nav>
			<?php } ?>
		</div>
	</header>

	<main>