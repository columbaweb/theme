</main>

<footer id="colophon">
	<div class="wrap">
		<div class="columns">
			<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
                <div class="col col-1">
					<div class="wb-1">
	                    <?php dynamic_sidebar( 'footer-1' ); ?>
						<p class="copy">Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
						<?php
						    if( (!empty(get_theme_mod('footer_copy'))) ) {
						        echo '<p class="copy">'. get_theme_mod('footer_copy'). '</p>';
						} ?>
					</div>
					
					<div class="wb-2">
						<img src="https://cdn.whitbread.co.uk/media/2021/05/11101800/top-employer-sm.png" width="500" height="229" alt="" />
						<a class="btn style-white" href="https://www.whitbreadcareers.com/" target="_blank">Visit careers site <svg role="img" aria-hidden="true" width="13" height="11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 1.415l4 4-4 4" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path><path d="M11 5.414H1" stroke="#fff" stroke-width="2" stroke-linecap="round"></path></svg></a>
					</div>
                </div>
            <?php endif; ?>

            <?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
                <div class="col col-2">
                    <?php dynamic_sidebar( 'footer-2' ); ?>
                </div>
            <?php endif; ?>

            <?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
                <div class="col col-3">
                    <?php dynamic_sidebar( 'footer-3' ); ?>
                </div>
            <?php endif; ?>
			
		</div>
	</div>
	
	<?php if(true === get_theme_mod('show_backtotop')) { ?>
		<a class="cd-top" href="#top">
			<span class="dashicons dashicons-arrow-up"></span>
			<span class="screen-reader-text">Back to top</span>
		</a>
	<?php } ?>
	
</footer>

<?php wp_footer(); ?>

<span id="nw" hidden>Opens in a new window</span>

</body>
</html>