<div class="post">
	<?php get_template_part( 'template-parts/content', 'postmeta' ); ?>
	<h3><a class="post-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	<p class="excerpt"><?php echo excerpt(50); ?></p>
</div>