<?php 

if ( 0 == $post->post_parent ) { ?>
    <h2 class="side-title is-style-border"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
<?php } else {
    $parents = get_post_ancestors( $post->ID ); ?>
    <h2 class="side-title is-style-border"><a href="<?= apply_filters( "the_title", get_permalink( end ( $parents ) ) ); ?>"><?= apply_filters( "the_title", get_the_title( end ( $parents ) ) ); ?></a></h2>
<?php } ?>