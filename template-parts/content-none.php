<article <?php post_class('post not-found'); ?>>
	
	<h2><?php esc_html_e( 'Nothing Found', 'bir' ); ?></h2>
	
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) :
		printf(
			'<p>' . wp_kses(
				__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'bir' ),
				array(
					'a' => array(
						'href' => array(),
					),
				)
			) . '</p>',
			esc_url( admin_url( 'post-new.php' ) )
		);

	elseif ( is_search() ) :  ?>

	<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with.', 'bir' ); ?></p>
		
	<?php get_search_form(); else : ?>

	<p><?php esc_html_e( 'It seems we can&rsquo;t find what you are looking for. Perhaps searching can help.', 'bir' ); ?></p>
	
	<?php 
		get_search_form();
		endif;
	?>

</article>