<?php
	global $post;
	$parentID = wp_get_post_parent_id(get_the_ID());

	if ( get_field( 'page_menu' ) ) {
	
		if( is_page() && $parentID == 0 ) {
			if (has_children()) {
				echo '<ul class="menu">';
			    wp_list_pages( array(
			        'title_li'    => '',
					'child_of'    => $post->ID,
			    ) );
			   	echo '</ul>';
			}
	    
	  	} else {
			echo '<ul class="menu">';
			wp_list_pages( array(
			    'title_li'    => '',
				'child_of'    => $post->post_parent,
			) );
			echo '</ul>';
	  	}
	  	
	} // show / hide menu
	  
?>