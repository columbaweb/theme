<?php

$countries = array(
"US" => "United States",
	"GB" => "United Kingdom",
	"AF" => "Afghanistan",
	"AL" => "Albania",
	"DZ" => "Algeria",
	"AS" => "American Samoa",
	"AD" => "Andorra",
	"AO" => "Angola",
	"AI" => "Anguilla",
	"AQ" => "Antarctica",
	"AG" => "Antigua And Barbuda",
	"AR" => "Argentina",
	"AM" => "Armenia",
	"AW" => "Aruba",
	"AU" => "Australia",
	"AT" => "Austria",
	"AZ" => "Azerbaijan",
	"BS" => "Bahamas",
	"BH" => "Bahrain",
	"BD" => "Bangladesh",
	"BB" => "Barbados",
	"BY" => "Belarus",
	"BE" => "Belgium",
	"BZ" => "Belize",
	"BJ" => "Benin",
	"BM" => "Bermuda",
	"BT" => "Bhutan",
	"BO" => "Bolivia",
	"BA" => "Bosnia And Herzegowina",
	"BW" => "Botswana",
	"BV" => "Bouvet Island",
	"BR" => "Brazil",
	"IO" => "British Indian Ocean Territory",
	"BN" => "Brunei Darussalam",
	"BG" => "Bulgaria",
	"BF" => "Burkina Faso",
	"BI" => "Burundi",
	"KH" => "Cambodia",
	"CM" => "Cameroon",
	"CA" => "Canada",
	"CV" => "Cape Verde",
	"KY" => "Cayman Islands",
	"CF" => "Central African Republic",
	"TD" => "Chad",
	"CL" => "Chile",
	"CN" => "China",
	"CX" => "Christmas Island",
	"CC" => "Cocos (Keeling) Islands",
	"CO" => "Colombia",
	"KM" => "Comoros",
	"CG" => "Congo",
	"CD" => "Congo, The Democratic Republic Of The",
	"CK" => "Cook Islands",
	"CR" => "Costa Rica",
	"CI" => "Cote D'Ivoire",
	"HR" => "Croatia (Local Name: Hrvatska)",
	"CU" => "Cuba",
	"CY" => "Cyprus",
	"CZ" => "Czech Republic",
	"DK" => "Denmark",
	"DJ" => "Djibouti",
	"DM" => "Dominica",
	"DO" => "Dominican Republic",
	"TP" => "East Timor",
	"EC" => "Ecuador",
	"EG" => "Egypt",
	"SV" => "El Salvador",
	"GQ" => "Equatorial Guinea",
	"ER" => "Eritrea",
	"EE" => "Estonia",
	"ET" => "Ethiopia",
	"FK" => "Falkland Islands (Malvinas)",
	"FO" => "Faroe Islands",
	"FJ" => "Fiji",
	"FI" => "Finland",
	"FR" => "France",
	"FX" => "France, Metropolitan",
	"GF" => "French Guiana",
	"PF" => "French Polynesia",
	"TF" => "French Southern Territories",
	"GA" => "Gabon",
	"GM" => "Gambia",
	"GE" => "Georgia",
	"DE" => "Germany",
	"GH" => "Ghana",
	"GI" => "Gibraltar",
	"GR" => "Greece",
	"GL" => "Greenland",
	"GD" => "Grenada",
	"GP" => "Guadeloupe",
	"GU" => "Guam",
	"GT" => "Guatemala",
	"GN" => "Guinea",
	"GW" => "Guinea-Bissau",
	"GY" => "Guyana",
	"HT" => "Haiti",
	"HM" => "Heard And Mc Donald Islands",
	"VA" => "Holy See (Vatican City State)",
	"HN" => "Honduras",
	"HK" => "Hong Kong",
	"HU" => "Hungary",
	"IS" => "Iceland",
	"IN" => "India",
	"ID" => "Indonesia",
	"IR" => "Iran (Islamic Republic Of)",
	"IQ" => "Iraq",
	"IE" => "Ireland",
	"IL" => "Israel",
	"IT" => "Italy",
	"JM" => "Jamaica",
	"JP" => "Japan",
	"JO" => "Jordan",
	"KZ" => "Kazakhstan",
	"KE" => "Kenya",
	"KI" => "Kiribati",
	"KP" => "Korea, Democratic People's Republic Of",
	"KR" => "Korea, Republic Of",
	"KW" => "Kuwait",
	"KG" => "Kyrgyzstan",
	"LA" => "Lao People's Democratic Republic",
	"LV" => "Latvia",
	"LB" => "Lebanon",
	"LS" => "Lesotho",
	"LR" => "Liberia",
	"LY" => "Libyan Arab Jamahiriya",
	"LI" => "Liechtenstein",
	"LT" => "Lithuania",
	"LU" => "Luxembourg",
	"MO" => "Macau",
	"MK" => "Macedonia, Former Yugoslav Republic Of",
	"MG" => "Madagascar",
	"MW" => "Malawi",
	"MY" => "Malaysia",
	"MV" => "Maldives",
	"ML" => "Mali",
	"MT" => "Malta",
	"MH" => "Marshall Islands",
	"MQ" => "Martinique",
	"MR" => "Mauritania",
	"MU" => "Mauritius",
	"YT" => "Mayotte",
	"MX" => "Mexico",
	"FM" => "Micronesia, Federated States Of",
	"MD" => "Moldova, Republic Of",
	"MC" => "Monaco",
	"MN" => "Mongolia",
	"MS" => "Montserrat",
	"MA" => "Morocco",
	"MZ" => "Mozambique",
	"MM" => "Myanmar",
	"NA" => "Namibia",
	"NR" => "Nauru",
	"NP" => "Nepal",
	"NL" => "Netherlands",
	"AN" => "Netherlands Antilles",
	"NC" => "New Caledonia",
	"NZ" => "New Zealand",
	"NI" => "Nicaragua",
	"NE" => "Niger",
	"NG" => "Nigeria",
	"NU" => "Niue",
	"NF" => "Norfolk Island",
	"MP" => "Northern Mariana Islands",
	"NO" => "Norway",
	"OM" => "Oman",
	"PK" => "Pakistan",
	"PW" => "Palau",
	"PA" => "Panama",
	"PG" => "Papua New Guinea",
	"PY" => "Paraguay",
	"PE" => "Peru",
	"PH" => "Philippines",
	"PN" => "Pitcairn",
	"PL" => "Poland",
	"PT" => "Portugal",
	"PR" => "Puerto Rico",
	"QA" => "Qatar",
	"RE" => "Reunion",
	"RO" => "Romania",	
	"RU" => "Russia",
	"RW" => "Rwanda",
	"KN" => "Saint Kitts And Nevis",
	"LC" => "Saint Lucia",
	"VC" => "Saint Vincent And The Grenadines",
	"WS" => "Samoa",
	"SM" => "San Marino",
	"ST" => "Sao Tome And Principe",
	"SA" => "Saudi Arabia",
	"SN" => "Senegal",
	"SC" => "Seychelles",
	"SL" => "Sierra Leone",
	"SG" => "Singapore",
	"SK" => "Slovakia (Slovak Republic)",
	"SI" => "Slovenia",
	"SB" => "Solomon Islands",
	"SO" => "Somalia",
	"ZA" => "South Africa",
	"GS" => "South Georgia, South Sandwich Islands",
	"ES" => "Spain",
	"LK" => "Sri Lanka",
	"SH" => "St. Helena",
	"PM" => "St. Pierre And Miquelon",
	"SD" => "Sudan",
	"SR" => "Suriname",
	"SJ" => "Svalbard And Jan Mayen Islands",
	"SZ" => "Swaziland",
	"SE" => "Sweden",
	"CH" => "Switzerland",
	"SY" => "Syrian Arab Republic",
	"TW" => "Taiwan",
	"TJ" => "Tajikistan",
	"TZ" => "Tanzania, United Republic Of",
	"TH" => "Thailand",
	"TG" => "Togo",
	"TK" => "Tokelau",
	"TO" => "Tonga",
	"TT" => "Trinidad And Tobago",
	"TN" => "Tunisia",
	"TR" => "Turkey",
	"TM" => "Turkmenistan",
	"TC" => "Turks And Caicos Islands",
	"TV" => "Tuvalu",
	"UG" => "Uganda",
	"UA" => "Ukraine",
	"AE" => "United Arab Emirates",
	"US" => "United States",
	"UM" => "United States Minor Outlying Islands",
	"UY" => "Uruguay",
	"UZ" => "Uzbekistan",
	"VU" => "Vanuatu",
	"VE" => "Venezuela",
	"VN" => "Viet Nam",
	"VG" => "Virgin Islands (British)",
	"VI" => "Virgin Islands (U.S.)",
	"WF" => "Wallis And Futuna Islands",
	"EH" => "Western Sahara",
	"YE" => "Yemen",
	"YU" => "Yugoslavia",
	"ZM" => "Zambia",
	"ZW" => "Zimbabwe"
);

?>

<div class="country_box">
	<h3>Disclaimer</h3>

	<p><strong>ELECTRONIC VERSIONS OF THE MATERIALS YOU ARE SEEKING TO ACCESS ARE BEING MADE AVAILABLE ON THIS WEBPAGE BY WHITE PLC (“WHITE” OR THE “COMPANY”) FOR INFORMATION PURPOSES ONLY.</strong></p>

	<p><strong>DUE TO RESTRICTIONS UNDER APPLICABLE SECURITIES LAWS, THESE MATERIALS ARE RESTRICTED AND ARE NOT INTENDED FOR, AND MUST NOT BE ACCESSED BY, OR DISTRIBUTED OR DISSEMINATED TO, PERSONS RESIDENT OR PHYSICALLY LOCATED IN CERTAIN JURISDICTIONS.</strong></p>

	<p>Please select the country from which you are seeking to access the materials: </p>

	<p>By clicking the "Continue" button below you certify that you understand this disclaimer and that you are seeking to access the materials from the country you selected.</p>

	<div class="country-select">
		<select class="country_select">
			<?php foreach ($countries as $code => $country) { ?>
				<option value="<?= $code ?>"><?= $country ?></option>
			<?php } ?>
		</select>
	
		<input type="submit" class="submit_country" value="Continue">
	</div>
	
</div>



<div class="access_denied">
	<h3>Access denied</h3>
	<p><strong>THESE MATERIALS ARE RESTRICTED AND ARE NOT INTENDED FOR, AND MUST NOT BE ACCESSED BY, OR DISTRIBUTED OR DISSEMINATED TO, PERSONS RESIDENT OR PHYSICALLY LOCATED IN YOUR JURISDICTION.</strong></p>
	<p>You have been prevented from obtaining access to the information contained herein due to the securities laws of the country in which you are located or resident.</p>
	<p>Please click <a href="<?php bloginfo('url'); ?>/investors">here</a> to return to the Investor section of our website.</p>
</div>

<div class="access_us">
	<h3>IMPORTANT INFORMATION</h3>
	
	<p>You have indicated that you are located in the United States. These materials are not intended for, directed at or accessible by persons located in the United States. However, persons located in the United States who make the certifications below can access these materials. Please read the certifications below carefully and provide the information requested in order to receive these materials. If you cannot make the certifications below, please choose “I do not agree” below.</p>
	
	<h4>Certifications</h4>
	
	<p><strong>I am a “qualified institutional buyer” (a “QIB”) as defined in Rule 144A (“Rule 144A”) under the U.S. Securities Act of 1933, as amended (the “US Securities Act”). Further, if I am acting as a fiduciary or agent for one or more investor accounts, (a) each such account is a QIB, (b) I have sole investment discretion with respect to each account, and (c) I have full power and authority to make the representations, warranties, agreements and acknowledgements herein on behalf of each such account.</strong></p>
	
	<p><strong>I acknowledge that the materials relate to a transaction that is not subject to, or is only available in the United States pursuant to an exemption from, the registration requirements of the US Securities Act.</strong></p>
	
	<p>Please fill in all fields below. All fields are mandatory.</p>
	
	<p>Your data will be held by the Company and processed only to ensure our compliance with the applicable regulations.</p>
	
	<?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true" tabindex="49"]'); ?>
	
	<div class="access_us_footer">
		<p>By clicking “I AGREE” below, you are certifying that the certifications and information provided above are accurate and that you would like to access the materials. You agree that the materials you receive are for your own use and will not be distributed to any person outside of your organisation.</p>
		
		<ul class="popup-controls">
			<li><button type="button" class="btn style-col pum-close popmake-close" disabled aria-label="I accept">I Agree</button></li>
			<li><button type="button" class="btn style-outline-col decline">I Disagree</button></li>
		</ul>
		
	</div>
</div>



<div class="disclaimer">
	<h3>Important information</h3>
	
	<p><strong>Please read this notice carefully - it applies to all persons who view this webpage.  Please note that the notice set out below may be altered or updated. You should read it in full each time you visit the site.</strong></p>
	
	<p><strong>ELECTRONIC VERSIONS OF THE MATERIALS YOU ARE SEEKING TO ACCESS ARE BEING MADE AVAILABLE ON THIS WEBSITE BY WHITE IN GOOD FAITH AND ARE FOR INFORMATION PURPOSES ONLY.</strong></p>
	
	<p><strong>THESE MATERIALS ARE RESTRICTED AND ARE NOT INTENDED FOR, AND MUST NOT BE ACCESSED BY, OR DISTRIBUTED OR DISSEMINATED TO, DIRECTLY OR INDIRECTLY, PERSONS LOCATED OR RESIDENT IN (OR ORGANISED OR INCORPORATED UNDER THE LAWS OF) IN THE UNITED STATES OF AMERICA (EXCEPT QUALIFIED INSTITUTIONAL BUYERS WITHIN THE MEANING OF THE U.S. SECURITIES ACT OF 1933, AS AMENDED (THE “US SECURITIES ACT”)), AUSTRALIA, SOUTH AFRICA, NEW ZEALAND, JAPAN, SINGAPORE, THE UNITED ARAB EMIRATES, OR ANY OTHER JURISDICTIONS WHERE THE EXTENSION OF AVAILABILITY OF THE MATERIALS TO WHICH YOU ARE SEEKING ACCESS WOULD BREACH ANY APPLICABLE LAW OR REGULATION.</strong></p>
	
	<h4>Overseas persons</h4>
	
	<p>Viewing the materials you seek to access in jurisdictions other than the United Kingdom may be prohibited or restricted by applicable law and regulation.  In some of these jurisdictions, only certain categories of persons may be allowed to view such materials.  All persons resident outside the United Kingdom who wish to view these materials must first satisfy themselves that they are not subject to any local requirements that prohibit or restrict them from doing so and should inform themselves of, and observe, any applicable legal or regulatory requirements applicable in their jurisdiction. It is your responsibility to satisfy yourself as to the full observance of any such legal and regulatory requirements.  If any such prohibition applies or you are in any doubt as to whether any such prohibition applies, please exit this webpage.</p>
	
	<p>The webpage and the information and materials contained herein do not constitute or form a part of any offer to sell or a solicitation of an offer to purchase or subscribe for securities in any jurisdiction or jurisdictions in which such offers, sales or solicitations are unlawful prior to registration or qualification under the securities laws of any such jurisdiction.</p>
	
	<p>Except as otherwise provided for herein, the materials contained on this webpage do not constitute or form a part of any offer or solicitation to purchase or subscribe for of Nil Paid Rights, Fully Paid Rights or New Ordinary Shares in the United States. None of the Nil Paid Rights, the Fully Paid Rights or the New Ordinary Shares have been or will be registered under the U.S. Securities Act or under any applicable securities laws of any state or other jurisdiction of the United States, and may not be offered, sold, pledged, taken up, exercised, resold, renounced, transferred or delivered, directly or indirectly, in, into or within the United States, except pursuant to an applicable exemption from, or in a transaction not subject to, the registration requirements of the US Securities Act and in compliance with any applicable securities laws of any state or other jurisdiction of the United States.  There will be no public offer of Securities in the United States. Shareholders in the United States must complete and return to the Company an investor letter in the appropriate form as described in the materials to exercise their rights in connection with the Rights Issue.</p>
	
	<p>Any relevant securities registration or other clearances have not been and will not be made or obtained with or from the relevant authorities in any other jurisdiction except the United Kingdom.  Accordingly, unless an exemption under the relevant securities law is applicable, the Nil Paid Rights, the Fully Paid Rights and the New Ordinary Shares may not be offered, sold, resold, delivered or distributed, directly or indirectly, in or into any other jurisdiction if to do so would constitute a violation of any applicable law of, or require registration of such securities in, such jurisdiction.</p>
	
	<h4>Member Sates of the EEA (other than the United Kingdom)</h4>
	
	<p>In relation to each member state of the European Economic Area (except the United Kingdom) (each, a “<strong>Relevant Member State</strong>”) the materials included on this webpage are only addressed to and directed at: (i) any legal entity which is a “<strong>qualified investor</strong>” within the meaning of Article 2(e) of the Prospectus Regulation (Regulation 2017/1129/EU) (“Qualified Investors”); (ii) to fewer than 150 natural or legal persons (other than Qualified Investors) per Relevant Member State, subject to obtaining the prior consent of the Banks (as defined below); or (iii) in any other circumstances falling within Article 1(4) of the Prospectus Regulation. Any investment or investment activity to which the materials relate is available only to the persons referenced above and will only be engaged with such persons. Persons who do not fall into one of the above categories should not act or rely on the materials or any of its contents.</p>
	
	<h4>Switzerland</h4>
	
	<p>The offering of the securities in Switzerland is exempt from the requirement to prepare and publish a prospectus under the Swiss Financial Services Act ("<strong>FinSA</strong>") because it is made to a limited number of persons which is less than 500 and falls within the exemption of article 36 section 1 lit. b) FinSA) and the securities will not be admitted to trading on any trading venue (exchange or multilateral trading facility in Switzerland). The materials contained in this webpage do not constitute a prospectus pursuant to the FinSA, and no such prospectus has been or will be prepared for or in connection with the Rights Issue.</p>
	
	<h4>Canada</h4>
	
	<p>The materials contained in this webpage and the pages that follow are not, and under no circumstances are to be construed as, a prospectus, an advertisement or a public offering of securities in Canada. No Canadian securities regulatory authority has expressed an opinion about the securities offered and it is an offence to claim otherwise.</p>
	
	<p>The distribution of the securities offered in Canada is being made only on a private placement basis exempt from the requirement that the Company prepare and file a prospectus with the applicable securities regulatory authorities. The Company is not a reporting issuer in any province or territory in Canada, its securities are not listed on any stock exchange in Canada and there is currently no public market for the securities offered in Canada. The Company currently has no intention of becoming a reporting issuer in Canada, filing a prospectus with any securities regulatory authority in Canada to qualify the resale of the securities offered to the public or listing its securities on any stock exchange in Canada.</p>
	
	<p>By clicking on the "I ACCEPT" button, Canadian investors seeking access to this webpage represent and warrant to the Company that they are current shareholders of the Company or are otherwise permitted to participate in the proposed offering under applicable Canadian securities laws on a prospectus and registration exempt basis, without the benefit of a prospectus, or similar document, qualified under such securities laws.</p>
	
	<h4>Hong Kong</h4>
	
	<p>The offering of securities in Hong Kong is only (i) to “professional investors” as defined in the Securities and Futures Ordinance (Cap.571, Laws of Hong Kong) of Hong Kong (the “<strong>SFO</strong>”) and any rule made under the SFO; or (ii) in other circumstances which do not constitute an offer to the public within the meaning of the Companies (Winding up and Miscellaneous Provisions) Ordinance (Cap.32, Laws of Hong Kong) of Hong Kong (the “<strong>C(WUMP)O</strong>”) or an invitation to induce an offer by the public to subscribe for or purchase any shares and which do not result in the materials in this webpage and the pages that follow being a “prospectus” as defined in the C(WUMP)O. No advertisement, invitation or document relating to the offering of securities may be issued or may be in the possession of any person for the purpose of issue, whether in Hong Kong or elsewhere, which is directed at, or the contents of which are likely to be accessed or read by, the public of Hong Kong (except if permitted to do so under the securities laws of Hong Kong) other than with respect to securities which are or are intended to be disposed of only to persons outside Hong Kong or only to “professional investors” as defined in the SFO and any rules made under the SFO or in other circumstances which do not constitute an offer or invitation to the public within the meaning of C(WUMP)O.</p>
	
	<p>The materials in this webpage and the pages that follow have not been reviewed by any regulatory authority in Hong Kong. Investors in Hong Kong are advised to exercise caution in relation to the offering of securities. If you are in any doubt about the materials in this webpage and the pages that follow, you should obtain professional independent advice.</p>
	
	<p>If you are not permitted to view the materials on this webpage or are in any doubt as to whether you are permitted to view these materials, please exit this webpage.</p>
	
	<h4>Basis of access</h4>
	
	<p>Access to electronic versions of the materials is being made available on this webpage by Whitbread for information purposes only and such materials are not comprehensive.  Any person seeking access to this webpage represents and warrants to Whitbread that they are doing so for information purposes only.  Making press announcements and other documents available in electronic format does not constitute an offer to sell or the solicitation of an offer to buy or subscribe for securities in Whitbread or constitute a recommendation by Whitbread or any other party to sell or buy securities in Whitbread.</p>
	
	<p>Although care has been taken by the Company in the preparation of the information, not all such information may be accurate and up to date in all respects.  No reliance may be placed for any purpose whatsoever on the information or opinions contained in the materials being made available on this webpage or any other document or oral statement or on the completeness, accuracy or fairness of such information and/or opinions therein. No representation or warranty, express or implied, is given by or on behalf of Whitbread or either J.P. Morgan Cazenove or Morgan Stanley (together the “<strong>Banks</strong>”) or any of such persons’ affiliates or their respective directors, officers, employees, agents or advisers or any other person as to the accuracy, timeliness, completeness, non-infringement, merchantability or fitness for any particular purpose of the information or opinions contained on this webpage and no liability whatsoever is accepted by Whitbread or either of the Banks or any of such persons’ affiliates or their respective directors, officers, employees, agents or advisers or any other person for any loss howsoever arising, directly or indirectly, from any use of such information or opinions or otherwise arising in connection therewith. Each of the Company and the Banks does not accept any responsibility, obligation or liability whatsoever for any direct or indirect or consequential loss or damages of any kind resulting from any use of or reliance on this webpage or any information contained in it.</p>
	
	<p>Please note that the information appearing in the materials is accurate only as of the date of the relevant document, regardless of the time of delivery of the document or of any offer or sale of the New Ordinary Shares.  Each of Whitbread and the Banks expressly disclaims any duty to provide any additional information, update or keep current information contained in the materials, or to remedy any omissions in or from the materials and any opinions expressed in the materials except as required by applicable law.</p>
	
	<h4>General</h4>
	
	<p>Certain elements of, and statements in, the materials in this webpage relate to the future, including forward-looking statements relating to the Group’s financial position and strategy. These statements, including the explanatory wording in the materials in this webpage in relation to the Company’s working capital, relate to future events or the future performance of the Company but no not seek in any way to qualify the working capital statement given by the Company. In some cases, these forward-looking statements can be identified by the use of forward-looking terminology, including the terms ‘intend’, ‘aim’, ‘project’, ‘anticipate’, ‘estimate’, ‘plan’, ‘believe’, ‘expect’, ‘may’, ‘should’, ‘will’, ‘continue’ or, in each case, their negative and other variations or other similar or comparable words and expressions. These statements discuss future expectations concerning the Group’s results of operations or financial condition, or provide other forward-looking statements.</p>

	<p>These forward-looking statements are not guarantees or predictions of future performance, and are subject to known and unknown risks, uncertainties and other factors, including the risk factors set out in the materials in this webpage, many of which are beyond the Group’s control, and which may cause the Group’s actual results of operations, financial condition and the development of the business sectors in which the Group operates to differ materially from those suggested by the forward-looking statements contained in the materials in this webpage.  In addition, even if the Group’s actual results of operations, financial condition and the development of the business sectors in which it operates are consistent with the forward-looking statements contained in the materials in this webpage, those results or developments may not be indicative of results or developments in subsequent periods.  Viewers of the materials in this webpage are cautioned not to put undue reliance on forward-looking statements.</p>
	
	<p>Other than as required by English law, none of the Company, its officers, advisers or any other person gives any representation, assurance or guarantee that the occurrence of the events expressed or implied in any forward-looking statements in the materials contained in this webpage will actually occur, in part or in whole.</p>
	
	<p>Additionally, statements of the intentions of the Board and/or Directors reflect the present intentions of the Board and/or Directors, respectively, as at the date of the materials contained in this webpage and may be subject to change as the composition of the Board alters, or as circumstances require.</p>
	
	<p>The forward-looking statements speak only as at the date of the materials contained in this webpage. To the extent required by applicable law or regulation (including as may be required by the Companies Act, Prospectus Regulation Rules, Listing Rules, MAR, Disclosure Guidance and Transparency Rules and FSMA), the Company will update or revise the information contained in the materials in this webpage.  Otherwise, the Company as well as the Banks expressly disclaim any obligation or undertaking to release publicly any updates or revisions to any forward-looking statements contained in the materials in this webpage to reflect any change in expectations with regard thereto or any change in events, conditions or circumstances on which any such statement is based. You should not place reliance on forward-looking statements, which speak only as at the date of the materials.</p>
	
	<p>Any other information contained in, or that can be accessed via, our website does not constitute a part of the materials.</p>
	
	<p>You are responsible for protecting against viruses and other destructive items. Your use of this webpage is at your own risk and it is your responsibility to take precautions to ensure that it is free from viruses or other items of a destructive nature.</p>
	
	<p>By clicking on the “Agree” button, you confirm that you have read and understood the information set forth above and agree to be bound by its terms, that you are permitted to proceed to electronic versions of the materials, and that breach of these certifications could mean that you are in breach of applicable laws or regulations.</p>

	<ul class="popup-controls">
		<li><button type="button" class="btn style-col pum-close popmake-close" aria-label="I accept">Agree</button></li>
		<li><button type="button" class="btn style-outline-col decline">Disagree</button></li>
	</ul>

</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('.access_denied').hide();
		jQuery('.disclaimer').hide();
		jQuery('.access_us').hide();
		
		jQuery('.pps-disclaimer-disagree').hide();
		jQuery('.pps-disclaimer-agree').hide();
		

		jQuery('.submit_country').click(function() {
			var country = jQuery('.country_select option:selected').val();

			if (country == 'AU' || country == 'ZA' || country == 'NZ' || country == 'JP' || country == 'SG' || country == 'SG' || country == 'AE') {
				jQuery('.country_box').hide();
				jQuery('.access_denied').show();
			
			} else if (country == 'US') {
				jQuery('.country_box').hide();
				jQuery('.access_us').show();
					
			} else {
				jQuery('.country_box').hide();
				jQuery('.disclaimer').show();
			}
		});
		
		jQuery('.btn.decline').click(function() {
			jQuery('.disclaimer').hide();
			jQuery('.access_us').hide();
			jQuery('.access_denied').show();
		});
		
		jQuery('#gform_submit_button_1').click(function() {
			//jQuery('#gform_1').hide();
			//jQuery('.access_us_footer').show();
			//jQuery('.access_us_footer .btn.style-col').removeAttr("disabled");
		});
		
		
		jQuery(document).bind('gform_confirmation_loaded', function(event, form_id){
			//$('#form').delay(3000).fadeOut(600);
			jQuery('.access_us_footer .btn.style-col').removeAttr("disabled");
		});
		
		
		
	});
</script>