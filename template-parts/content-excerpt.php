<div class="post p-<?php echo $class;?>">
			
	<?php if(has_post_thumbnail()) { ?>
	<span class="bg" style="background-image: url(<?php the_post_thumbnail_url($post->ID, 'full'); ?>);"></span>
	<?php } else { ?>
	<span class="bg" style="background-image: url(<?php bloginfo('template_url') ?>/assets/images/whitbread-placeholder.svg);"></span>
	<?php } ?>

	<?php get_template_part( 'template-parts/content', 'postmeta' ); ?>
	
	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

	<p class="excerpt"><?php echo excerpt(50); ?></p>
</div>