<?php if (get_field('slider_type') == 'slider') { ?>

<div id="banner" class="slider autoplay">
	
	<?php if( have_rows('slider_builder') ): ?>
		<?php while( have_rows('slider_builder') ): the_row(); ?>
			
			<div class="slide<?php if( get_field('overlay_slider') == 'enable' ) { echo " overlay"; } ?>">
				<?php 
					$images = get_sub_field('slider_image');
					$rand = array_rand($images, 1);
				?>
				
				<span class="bg" style="background-image: url(<?= $images[$rand]['url']; ?>);"></span>
				
				<div class="wrap">
					<div class="slide-content">
						
						<?php if(get_sub_field('sldier_title' ) != ""): ?>
						    <h2><?php the_sub_field('sldier_title'); ?></h2>
						<?php endif; ?>
						
						<?php if(get_sub_field('slider_content' ) != ""): ?>
						    <?php the_sub_field('slider_content'); ?>
						<?php endif; ?>
						
						<?php 
							$link = get_sub_field('button');
							if( $link ): 
						    	$link_url = $link['url'];
						    	$link_title = $link['title'];
						    	$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						    <a class="btn style-outline-white" href="<?= esc_url( $link_url ); ?>" target="<?= esc_attr( $link_target ); ?>"><?= esc_html( $link_title ); ?><svg role="img" aria-hidden="true" width="13" height="11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 1.415l4 4-4 4" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M11 5.414H1" stroke="#fff" stroke-width="2" stroke-linecap="round"/></svg></a>
						<?php endif; ?>
						
						<?php if (get_sub_field('video_lightbox') ) { ?>
							<a class="btn style-outline-white" data-fancybox href="<?php the_sub_field('video_lightbox'); ?>"><?php the_sub_field('video_lightbox_link_text'); ?><svg role="img" aria-hidden="true" width="13" height="11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 1.415l4 4-4 4" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M11 5.414H1" stroke="#fff" stroke-width="2" stroke-linecap="round"/></svg></a>
						<?php } ?>
						
					</div>
				</div>
			</div>
		<?php endwhile; ?> 
	<?php endif; ?>
	
</div>

<?php } else if (get_field('slider_type') == 'banner') { ?>

	<div id="banner" class="bg <?php the_field('image_banner_size'); ?><?php if( get_field('overlay') == 'enable' ) { echo " overlay"; } ?>" style="background-image: url(<?php the_field('static_image_bg'); ?>); background-position: <?php the_field('bg_image_position'); ?>;">
	</div>

<?php } ?>