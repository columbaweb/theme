<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}
	
	$className = 'team';
	
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

	$category = get_field('team_category');
?>


<div class="<?php echo esc_attr($className); ?>">
	
	<?php
		$args = array(
			'post_type' => 'team',
			'posts_per_page' => -1,
			'order' => 'ASC',
			'tax_query' => array(
			    array(
			        'taxonomy' => 'team-category',
			        'field' => 'id',
					'terms'    => $category,
			    )
			)
		);
		$teamQuery = new WP_Query( $args ); 
		global $post;
	?>
	
	<?php if ($teamQuery->have_posts()): ?>
		<?php while ($teamQuery->have_posts()): $teamQuery->the_post(); ?>
			<div class="team-head">
				
				<?php 
					$image = get_field('image', $post);
					$size = 'team';
					if( $image ) {
						echo wp_get_attachment_image( $image, $size );
					} else {
						echo '<img src="'.get_stylesheet_directory_uri().'/assets/images/inc/team-profile.svg" width="700" height="440" alt="" />';
					}
				?>
				
				<div class="team-details">
					<h3 class="name">
						<?php the_title(); ?>
						<span class="pos"><?php the_field('position', $post); ?></span>
						<svg role="img" aria-hidden="true" width="13" height="11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 1.415l4 4-4 4" stroke="#012d73" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M11 5.414H1" stroke="#012d73" stroke-width="2" stroke-linecap="round"/></svg>
					</h3>
				</div>
			</div>
			
			<?php if ( !is_admin() ) { ?>
			<div class="team-bio">
				<div class="columns">
				
					<div class="col1">
						<?php the_field('biography', $post); ?>
					</div>
					
					<div class="col2">
						<?php the_field('additional_information', $post); ?>
					</div>
				</div>

			</div>
			<?php } ?>
			
		<?php endwhile; ?>
	<?php endif; wp_reset_query(); ?>

</div>

<?php if ( is_admin() ) { ?>
	<style type="text/css">
		.<?php echo $className; ?> .team-head {
			min-height: 80px;
			margin-bottom: 10px;
			padding: 15px 85px 15px 15px;
			background: #fafafa;
			position: relative;
		}
		.<?php echo $className; ?> .team-head h3 {
			font-size: 18px;
			margin: 0 0 5px 0;
		}
		.<?php echo $className; ?> .team-head p {
			font-size: 15px;
			font-weight: normal;
			margin: 0;
		}
		.<?php echo $className; ?> .team-head img {
			width: auto;
			height: 55px;
			margin: 0;
			position: absolute;
			top: 15px;
			right: 15px;
		}
	</style>
<?php } ?>

