<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}
	
	$className = 'events-widget';
	
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
?>

<div class="<?php echo esc_attr($className); ?>">
	
	<?php
		$today = date('Ymd');
		
		$args = array(
        	'post_type'  => 'events',
        	'posts_per_page'  => '1',
        	'meta_key'			=> 'event_date',
        	'orderby'			=> 'meta_value',
        	'order' => 'ASC',

            'meta_query' => array(
        		array(
        	        'key'		=> 'event_date',
        	        'compare'	=> '>=',
        	        'value'		=> $today,
        	    ),
            ),
        );

		$ueQuery = new WP_Query( $args ); 
		global $post;
	?>
	
	<?php if ($ueQuery->have_posts()): while ($ueQuery->have_posts()): $ueQuery->the_post(); ?>
		<div class="event">
			<?php
			    $today = date('Ymd');
			    $dateformatstring = "Ymd";
			    $dateformatstring2 = "d M Y";
			    
			    $unixtimestamp = strtotime(get_field('event_date', $post));
			    $calDate = date_i18n($dateformatstring, $unixtimestamp);
			    $calDateFront = date_i18n($dateformatstring2, $unixtimestamp);
			    
			    $unixtimestampend = strtotime(get_field('event_end_date', $post));
			    $calDateEnd = date_i18n($dateformatstring, $unixtimestampend);
			    $calDateFrontEnd = date_i18n($dateformatstring2, $unixtimestampend);
			    
			    $title=get_the_title();
			    $titleNoSpace = str_replace(' ', '%20', $title);
			?>

			<p class="event__title">
				<span class="meta">
					<?php if (get_field('event_date_tbc', $post)) { ?>
					TBC
					<?php } else { ?>
					<?php echo $calDateFront;?>	
					
					<?php if(get_field('event_end_date', $post ) != ""): ?>
					    - <?php echo $calDateFrontEnd; ?>
					<?php endif; ?>
					
					<?php } ?>
				</span>
				<strong><?php the_title(); ?></strong>
			</p>
			
			<?php if ( !is_admin() ) { ?>
				
				<?php if ($calDate >= $today) : ?>
					<?php if (!get_field('event_date_tbc', $post)) { ?>
					<div class="event-links">			
						<a class="ical" href="<?php echo get_feed_link('calendar'); ?>?id=<?php echo get_the_ID(); ?>">iCal</a>
						<?php
							if ( get_field('event_end_date', $post ) != "" ) {    
								$endd = $calDateEnd;
							} else { 
								$endd = $calDate;
							}
						?>
			
						<a class="gcal" target="_blank" title="Add to Google Calendar" href="http://www.google.com/calendar/event?action=TEMPLATE&text=<?php echo $titleNoSpace; ?>&dates=<?php echo $calDate;?>T080000Z/<?php echo $endd;?>T090000Z&details=&location=">Google</a>
						
					</div>
					<?php } ?>
				<?php endif; ?>
			<?php } ?>
		</div>

	<?php endwhile; else : ?>
	
	<p class="no-events">No upcoming events are currently scheduled</p>
	
	<?php endif; wp_reset_query(); ?>

</div>

<?php if ( is_admin() ) { ?>
	<style type="text/css">
		.upcoming .event {
			margin-bottom: 10px;
			padding: 15px;
			background: #fafafa;
		}
	</style>
<?php } ?>