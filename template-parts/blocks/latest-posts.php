<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

	$className = 'latest-posts';
	
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
	
	$type = get_field('block_type') ?: 'all';
	$category = get_field('category');
	$count = get_field('posts_to_display') ?: '3';
	$layout = get_field('layout') ?: 'fw';
?>

<div class="<?= esc_attr($className); ?> <?= $layout; ?>">
	
	<?php 
	
// category 	
		if( get_field('block_type') == 'cat' ) {
			$args = array(
				'post_type'			=> array( 'post' ),
				'ignore_sticky_posts' => 1,
				'posts_per_page'	=> $count,
				'cat' => $category,
			);
			$postsQuery = new WP_Query( $args );
			
		} else { 
// all		
			$args = array(
				'post_type'			=> array( 'post' ),
				'ignore_sticky_posts' => 1,
				'posts_per_page'	=> $count,
			);
			$postsQuery = new WP_Query( $args );
		} 
	?>

	<?php if ($postsQuery->have_posts()) : while ($postsQuery->have_posts()) : $postsQuery->the_post(); ?>
		<div class="post">
			
			<?php if(has_post_thumbnail()) { ?>
			<span class="bg" style="background-image: url(<?php the_post_thumbnail_url($post->ID, 'full'); ?>);"></span>
			<?php } else { ?>
			<span class="bg" style="background-image: url(<?php bloginfo('template_url') ?>/assets/images/whitbread-placeholder.svg);"></span>
			<?php } ?>

			<?php get_template_part( 'template-parts/content', 'postmeta' ); ?>
			
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			
			<?php if ( !is_admin() ) { ?>
			<p class="excerpt"><?= excerpt(55); ?></p>
			<?php } ?>	
		</div>		
	<?php endwhile; endif; wp_reset_query(); ?>

</div>

<?php if ( is_admin() ) { ?>
	<style type="text/css">
		.<?= $className; ?> .excerpt {
			margin: 0 0 10px 0;
			padding: 15px;
			background: #fafafa;
		}
		.<?= $className; ?> .excerpt h3 {
			font-size: 18px;
			margin: 0;
		}
		.<?= $className; ?> .excerpt h3 a {
			text-decoration: none;
			color: #444;
		}
	</style>
<?php } ?>