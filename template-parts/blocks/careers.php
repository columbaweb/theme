<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}
	
	$className = 'careers';
	
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

	$title = get_field('title');
	$intro = get_field('intro');
?>

<div class="<?= esc_attr($className); ?>">
	
	<?php if ($title) { ?>
		<h2><?= $title; ?></h2>
	<?php } ?>
	
	<?php if ($intro) { ?>
		<?= $intro; ?>
	<?php } ?>

	<?php
		$args = array(
			'post_type' => 'careers',
			'posts_per_page' => -1,
			'order' => 'ASC'
		);
		$careersQuery = new WP_Query( $args ); 
		global $post;
	?>
	
	<?php if ($careersQuery->have_posts()): ?>
		<?php while ($careersQuery->have_posts()): $careersQuery->the_post(); ?>
			<div class="vacancy">
				
				<h3><?php the_title(); ?></h3>
				
				<?php 
					if ( !is_admin() ) { 
						the_field('short_description', $post);
					} 
				?>
				
			</div>
		<?php endwhile; ?>
	<?php endif; wp_reset_query(); ?>

</div>

<?php if ( is_admin() ) { ?>
	<style type="text/css">
		.<?= $className; ?> .vacancy {
			margin-bottom: 10px;
			padding: 15px 85px 15px 15px;
			background: #fafafa;
		}
		.<?= $className; ?> .vacancy h3 {
			font-size: 18px;
			margin: 0;
		}
	</style>
<?php } ?>