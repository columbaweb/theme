<?php

/* Brands Carousel Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'project-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
	$className = '';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
?>

<?php if( have_rows('carousel') ): ?>
<div class="brands-carousel-wrap alignfull <?= $className; ?>">
	<div class="brands-carousel-wrap-inner">
	
		<div class="brands-carousel">
		    <?php while( have_rows('carousel') ): the_row(); ?>
			
			<div class="slide" data-title="<?php the_sub_field('title'); ?>">
				
				<span class="bg" style="background-image: url(<?php the_sub_field('background_image'); ?>);"></span>
				
				<div class="slide-content">
					<div class="slide-content-inner">
						<?php 
							$image = get_sub_field('logo');
							$size = 'full';
							if( $image ) {
							  echo wp_get_attachment_image( $image, $size );
							}
						?>
						
						<?php the_sub_field('content'); ?>
						
						<?php 
							$link = get_sub_field('button'); 
							if( $link ) {
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a class="btn style-link-col" href="<?= esc_url($link_url); ?>" target="<?= esc_attr($link_target); ?>">
							<?= esc_html($link_title); ?> <svg role="img" aria-hidden="true" width="13" height="11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 1.415l4 4-4 4" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M11 5.414H1" stroke="#fff" stroke-width="2" stroke-linecap="round"/></svg>
							</a>
						<?php }  ?>
					</div>
				</div>
				
			</div>
			
		    <?php endwhile; ?> 
				
		</div>
		
		<div id="carouselPager"></div>
	
	</div>
</div>
<?php endif; ?>