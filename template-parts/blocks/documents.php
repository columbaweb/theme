<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}
	
	$className = 'files';
	
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
	
	$category = get_field('category');
	
?>

<?php
	$args = array(
		'post_type' => 'documents',
		'posts_per_page' => -1,
			'order' => 'DESC',
			'tax_query' => array(
			    array(
			        'taxonomy' => 'document-type',
			        'field' => 'id',
					'terms'    => $category,
			    )
			)
		);
	$docsQuery = new WP_Query( $args ); 
	global $post;
	if ($docsQuery->have_posts()): 
?>
	<div class="<?php echo esc_attr($className); ?>">
		
		<?php if (get_field('table_title')) { ?>
		<h4><?php the_field('table_title'); ?></h4>
		<?php } ?>
		
		<?php if ( ( $category == 40 ) || ( $category == 41 ) || ( $category == 42 ) || ( $category == 43 ) || ( $category == 44 ) || ( $category == 45 ) || ( $category == 46 ) || ( $category == 47 ) || ( $category == 48 ) || ( $category == 49 ) || ( $category == 50 ) || ( $category == 51 ) || ( $category == 52 ) || ( $category == 53 ) || ( $category == 54 ) || ( $category == 55 ) || ( $category == 56 ) || ( $category == 57 ) || ( $category == 58 ) || ( $category == 59 ) || ( $category == 60 ) || ( $category == 61 ) || ( $category == 62 ) || ( $category == 63 )) { ?>
			
		<div class="row head">
			<div class="name">
				<span class="date cell"></span>
				<span class="title cell"></span>
			</div>
			
			<div class="results">
				<span class="cell report">Report</span>
				<span class="cell pr">Press release</span>
				<span class="cell webcast">Webcast</span>
				<span class="cell qa">Q&amp;A</span>
				<span class="cell presentation">Presentation</span>
				<span class="cell info">Supplementary Information</span>
			</div>
		</div>
		
		<?php } ?>
		
	
		<?php while ($docsQuery->have_posts()): $docsQuery->the_post(); ?>
		
			<?php if ( get_field('document_type', $post) == 'results' ) { ?>
				
				<div class="row file">
					
					<div class="name">
						<span class="date cell">
							<?php the_time('j M'); ?>
						</span>
						
						<span class="title cell">
							<?php the_title(); ?>
						</span>
					</div>
					
					<div class="results">
					
						<span class="cell report<?php if ( (!get_field('report', $post)) ) { echo " empty"; } ?>">
							<?php 
								if (get_field('report', $post)) { 
									echo '<span class="label">Report</span>';
									$file = get_field('report', $post);
									echo '<a href="'.$file.'" target="_blank">';		
									echo get_file_type($file);
									echo '</a>';
								} 
							?>
						</span>
						
						<span class="cell pr<?php if ( (!get_field('press_release_link', $post)) && (!get_field('press_release_pdf', $post)) ) { echo " empty"; } ?>">
							
							<?php 
								if (get_field('pr_type', $post) == 'url') {
									echo '<span class="label">Press Release</span>';
									if (get_field('press_release_link', $post)) {
										echo '<a href="'.get_field('press_release_link', $post).'" target="_blank">View</a>';
									}
								} elseif (get_field('pr_type', $post) == 'file') {
									echo '<span class="label">Press Release</span>';
									$file = get_field('press_release_pdf', $post);
									echo '<a href="'.$file.'" target="_blank">';		
									echo get_file_type($file);				
									echo '</a>';
								}
							?>	
						</span>
						
						<span class="cell webcast<?php if ( (!get_field('webcast_link', $post)) && (!get_field('webcast_file', $post)) ) { echo " empty"; } ?>">
							<?php 
								if (get_field('webcast_type', $post) == 'url') {
									echo '<span class="label">Webcast URL</span>';
									if (get_field('webcast_link', $post)) {
										echo '<a href="'.get_field('webcast_link', $post).'" target="_blank">View</a>';
									}
								} elseif (get_field('webcast_type', $post) == 'file') {
									echo '<span class="label">Webcast PDF</span>';
									$file = get_field('webcast_file', $post);
									echo '<a href="'.$file.'" target="_blank">';		
									echo get_file_type($file);				
									echo '</a>';
								}
							?>
							
						</span>
						
						<span class="cell qa<?php if ( (!get_field('qa_link', $post)) && (!get_field('qa_file', $post)) ) { echo " empty"; } ?>">
							<?php 
								if (get_field('qa_type', $post) == 'url') {
									echo '<span class="label">Q&amp;A</span>';
									if (get_field('qa_link', $post)) {
										echo '<a href="'.get_field('qa_link', $post).'" target="_blank">View</a>';
									}
								} elseif (get_field('qa_type', $post) == 'file') {
									echo '<span class="label">Q&amp;A</span>';
									$file = get_field('qa_file', $post);
									echo '<a href="'.$file.'" target="_blank">';						
									echo get_file_type($file);
									echo '</a>';
								}
							?>
						</span>
						
						<span class="cell presentation<?php if ( (!get_field('presentation', $post)) ) { echo " empty"; } ?>">
							<?php 
								if (get_field('presentation', $post)) { 
									echo '<span class="label">Presentation</span>';
									$file = get_field('presentation', $post);
									echo '<a href="'.$file.'" target="_blank">';						
									echo get_file_type($file);
									echo '</a>';
								} 
							?>
						</span>
						
						<span class="cell info<?php if ( (!get_field('info', $post)) ) { echo " empty"; } ?>">
							<?php 
								if (get_field('info', $post)) { 
									echo '<span class="label">Supplementary Information</span>';
									$file = get_field('info', $post);
									echo '<a href="'.$file.'" target="_blank">';						
									echo get_file_type($file);
									echo '</a>';
								} 
							?>
						</span>
					</div>

				</div>
				
				
			<?php } else { ?>

				<a class="file" href="<?php the_field('file', $post); ?>" target="_blank">
					<span class="title">
						<?php if (get_field('subtitle', $post)) { ?>
						<em><?php the_field('subtitle', $post); ?></em>
						<?php } ?>
						<?php the_title(); ?>
					</span>
					
					<span class="size">
						<?php 
							$file = get_field('file', $post);					
							echo '<em>'.get_file_type($file).'</em>';
							$attachment_id = attachment_url_to_postid( $file );
							$filesize = filesize( get_attached_file( $attachment_id ) );
							$filesize = size_format($filesize);
							echo '('.$filesize.')';
						?>
					</span>
				</a>
			
			<?php } ?>
				
		<?php endwhile; ?>
		
		
	</div>		
<?php endif; wp_reset_query(); ?>



<?php if ( is_admin() ) { ?>
	<style type="text/css">
		.<?php echo $className; ?> .file {
			display: block;
			margin-bottom: 10px;
			padding: 15px 85px 15px 15px;
			color: #191e23;
			background: #fafafa;
		}
	</style>
<?php } ?>