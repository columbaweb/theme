<?php
/* ADVISER Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
	$className = 'advisers';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

?>

<?php if( have_rows('advisers') ): ?>
	<div class="<?= $className; ?>">
	    <?php while( have_rows('advisers') ): the_row(); ?>
		
		<?php 
			$title = get_sub_field('title');
			$name = get_sub_field('name');
			$text = get_sub_field('adviser_info');
			$image = get_sub_field('logo');
			$size = 'full';
			$link = get_sub_field('url');
			
			if ($link) {
				$link_url = $link['url'];
				$link_title = $link['title'] ? : 'View site';
				$link_target = $link['target'] ? $link['target'] : '_self';
			}
		?>
			<div class="adviser">	
				<?php if( $image ) { ?>
					<?php if( $link ) { ?>
					<a class="logo" href="<?= esc_url($link_url); ?>" target="<?= esc_attr($link_target); ?>">
					<?php } ?>
						<?= wp_get_attachment_image( $image, $size ); ?>
					<?php if( $link ) { ?>
					</a>
					<?php } ?>
				<?php } ?>
				
				<h3 class="title is-style-border">
					<?php if( $link ) { ?><a href="<?= esc_url($link_url); ?>" target="<?= esc_attr($link_target); ?>"><?php } ?>
					<?= $title; ?>
					<?php if( $link ) { ?></a><?php } ?>
				</h3>
				
				<h4><?= $name; ?></h4>
				
				<?= $text; ?>
			</div>
		
	    <?php endwhile; ?> 
	</div>
<?php endif; ?>




<?php if ( is_admin() ) { ?>
	<style type="text/css">
		
		.<?= $className; ?> {
			border-top: 2px solid #1A1919;
		}
		
		.<?= $className; ?> .logo {
			margin: 5px 0 20px 0;
			padding: 10px 15px 8px 15px;
			background: #fafafb;
			border-bottom: 2px solid #f2b03d;
			position: relative;
			transition: background 0.2s ease-in-out;
		}
		
		.<?= $className; ?> .logo a {
			display: block;
			text-decoration: none;
			padding-right: 70px;
			color: #000;
		}
		
		.<?= $className; ?> .logo .site-btn {
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: center;
			-webkit-align-items: center;
			-ms-flex-align: center;
			align-items: center;
			-webkit-box-pack: center;
			-webkit-justify-content: center;
			-ms-flex-pack: center;
			justify-content: center;
			width: 70px;
			font-size: 11px;
			text-align: center;
			color: #fff;
			background: #8392a0;
			position: absolute;
			top: 0;
			bottom: 0;
			right: 0;
			transition: background 0.2s ease-in-out;
		}
		
		.<?= $className; ?> .logo .site-btn > span {
			display: block;
		}
		
		.<?= $className; ?> .logo .site-btn svg {
			display: block;
			width: 17px;
			height: 17px;
			margin: 4px auto 6px auto;
		}
	</style>
<?php } ?>
