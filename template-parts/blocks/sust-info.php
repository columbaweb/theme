<?php

/* Sustainability Infographic Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'project-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
	$className = 'sustainability-infographic';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
?>

<div class="<?php echo $className; ?>">
	
	<?php 
		$image = get_field('image');
		$size = 'full';
		if( $image ) {
			echo '<div class="info-head">';
			echo wp_get_attachment_image( $image, $size );
			echo '</div>';
		}
	?>
	
	<div class="panels">
	
		<div class="panel panel-opportunity">
	
			<?php if( have_rows('opportunity') ): ?>
				<?php while( have_rows('opportunity') ): the_row(); ?>
					
					<div class="title-wrap">
						<h2><?php the_sub_field('title'); ?></h2>
					</div>
					
					<?php if( have_rows('featured_nr_1') ): ?>
						<div class="counter-container n1">
							<div class="counter">
							<?php while( have_rows('featured_nr_1') ): the_row(); ?>
								<p class="number <?php the_sub_field('number_colour'); ?>">
						
									<?php if( get_sub_field('prefix') ) { ?>
										<span class="pref"><?php the_sub_field('prefix'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('number') ) { ?>
										<span class="count" data-from="0" data-to="<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('suffix') ) { ?>
										<span class="suf"><?php echo the_sub_field('suffix'); ?></span>
									<?php } ?>
								</p>
								
								<?php if( get_sub_field('label') ) { ?>
									<p class="title"><?php the_sub_field('label') ?></p>
								<?php } ?>
								
							<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
					
					<?php if( have_rows('featured_nr_2') ): ?>
						<div class="counter-container n2">
							<div class="counter">
							<?php while( have_rows('featured_nr_2') ): the_row(); ?>
								<p class="number <?php the_sub_field('number_colour'); ?>">
						
									<?php if( get_sub_field('prefix') ) { ?>
										<span class="pref"><?php the_sub_field('prefix'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('number') ) { ?>
										<span class="count" data-from="0" data-to="<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('suffix') ) { ?>
										<span class="suf"><?php echo the_sub_field('suffix'); ?></span>
									<?php } ?>
								</p>
								
								<?php if( get_sub_field('label') ) { ?>
									<p class="title"><?php the_sub_field('label') ?></p>
								<?php } ?>
								
							<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
					
					
					<?php if(get_sub_field('intro')) { ?>
						<div class="intro">
							<div class="panel-inner">
								<?php the_sub_field('intro'); ?>
							</div>
						</div>
					<?php } ?>
					
					<?php if(get_sub_field('content')) { ?>
						<div class="content">
							<div class="panel-inner">
								<?php the_sub_field('content'); ?>
							</div>
						</div>
					<?php } ?>
		
				<?php endwhile; ?>
			<?php endif; ?>
	
		</div>
		
		<div class="panel panel-community">
			<?php if( have_rows('community') ): ?>
				<?php while( have_rows('community') ): the_row(); ?>
					
					<div class="title-wrap">
						<h2><?php the_sub_field('title'); ?></h2>
					</div>
					
					<?php if( have_rows('featured_nr_1') ): ?>
						<div class="counter-container n1">
							<div class="counter">
							<?php while( have_rows('featured_nr_1') ): the_row(); ?>
								<p class="number <?php the_sub_field('number_colour'); ?>">
						
									<?php if( get_sub_field('prefix') ) { ?>
										<span class="pref"><?php the_sub_field('prefix'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('number') ) { ?>
										<span class="count" data-from="0" data-to="<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('suffix') ) { ?>
										<span class="suf"><?php echo the_sub_field('suffix'); ?></span>
									<?php } ?>
								</p>
								
								<?php if( get_sub_field('label') ) { ?>
									<p class="title"><?php the_sub_field('label') ?></p>
								<?php } ?>
								
							<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
					
					<?php if( have_rows('featured_nr_2') ): ?>
						<div class="counter-container n2">
							<div class="counter">
							<?php while( have_rows('featured_nr_2') ): the_row(); ?>
								<p class="number <?php the_sub_field('number_colour'); ?>">
						
									<?php if( get_sub_field('prefix') ) { ?>
										<span class="pref"><?php the_sub_field('prefix'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('number') ) { ?>
										<span class="count" data-from="0" data-to="<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('suffix') ) { ?>
										<span class="suf"><?php echo the_sub_field('suffix'); ?></span>
									<?php } ?>
								</p>
								
								<?php if( get_sub_field('label') ) { ?>
									<p class="title"><?php the_sub_field('label') ?></p>
								<?php } ?>
								
							<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
					
					
					<?php if(get_sub_field('intro')) { ?>
						<div class="intro">
							<div class="panel-inner">
								<?php the_sub_field('intro'); ?>
							</div>
						</div>
					<?php } ?>
					
					<?php if(get_sub_field('content')) { ?>
						<div class="content">
							<div class="panel-inner">
								<?php the_sub_field('content'); ?>
							</div>
						</div>
					<?php } ?>
		
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		
		<div class="panel panel-responsibility">
			<?php if( have_rows('responsibility') ): ?>
				<?php while( have_rows('responsibility') ): the_row(); ?>
					
					<div class="title-wrap">
						<h2><?php the_sub_field('title'); ?></h2>
					</div>
					
					<?php if( have_rows('featured_nr_1') ): ?>
						<div class="counter-container n1">
							<div class="counter">
							<?php while( have_rows('featured_nr_1') ): the_row(); ?>
								<p class="number <?php the_sub_field('number_colour'); ?>">
						
									<?php if( get_sub_field('prefix') ) { ?>
										<span class="pref"><?php the_sub_field('prefix'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('number') ) { ?>
										<span class="count" data-from="0" data-to="<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('suffix') ) { ?>
										<span class="suf"><?php echo the_sub_field('suffix'); ?></span>
									<?php } ?>
								</p>
								
								<?php if( get_sub_field('label') ) { ?>
									<p class="title"><?php the_sub_field('label') ?></p>
								<?php } ?>
								
							<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
					
					<?php if( have_rows('featured_nr_2') ): ?>
						<div class="counter-container n2">
							<div class="counter">
							<?php while( have_rows('featured_nr_2') ): the_row(); ?>
								<p class="number <?php the_sub_field('number_colour'); ?>">
						
									<?php if( get_sub_field('prefix') ) { ?>
										<span class="pref"><?php the_sub_field('prefix'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('number') ) { ?>
										<span class="count" data-from="0" data-to="<?php the_sub_field('number'); ?>"><?php the_sub_field('number'); ?></span>
									<?php } ?>
									
									<?php if( get_sub_field('suffix') ) { ?>
										<span class="suf"><?php echo the_sub_field('suffix'); ?></span>
									<?php } ?>
								</p>
								
								<?php if( get_sub_field('label') ) { ?>
									<p class="title"><?php the_sub_field('label') ?></p>
								<?php } ?>
								
							<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
					
					
					<?php if(get_sub_field('intro')) { ?>
						<div class="intro">
							<div class="panel-inner">
								<?php the_sub_field('intro'); ?>
							</div>
						</div>
					<?php } ?>
					
					<?php if(get_sub_field('content')) { ?>
						<div class="content">
							<div class="panel-inner">
								<?php the_sub_field('content'); ?>
							</div>
						</div>
					<?php } ?>
		
				<?php endwhile; ?>
			<?php endif; ?>
			
		</div>
	</div>
	
</div>




	
	
	
