<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

	$className = 'video-popup';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

	$url = get_field('video_url');
	$thumb = get_field('featured_image');
	$size = 'full';
?>

<div class="<?php echo esc_attr($className); ?>">
	<a data-fancybox href="<?php echo $url; ?>">
		<?php if( $thumb ) {
			echo wp_get_attachment_image( $thumb, $size );
		} ?>
		
		<?php if ( !is_admin() ) { ?>
		<svg role="img" width="68" height="68" xmlns="http://www.w3.org/2000/svg"><g fill-rule="nonzero" fill="none"><circle fill-opacity=".9" fill="#444" cx="34" cy="34" r="34"/><path fill="#FFF" d="M47.2 34L25.6 46V22z"/></g></svg>
		<?php } ?>
	</a>
</div>