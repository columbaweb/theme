<?php

/* CTA Box Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'bg-cta-box';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
	
	$bg = get_field('background_colour');
	$type = get_field('type');
	$layout = get_field('layout');
?>

<div class="<?= $className; ?> <?= $bg; ?> <?= $type; ?> <?= $layout; ?>">
	<?php if ( is_admin() ) { ?><p class="block-admin-label"><span>CTA block - click to add elements</span></p><?php } ?>
	
	<div class="inner">
		<InnerBlocks />
	</div>
	
</div>

<?php if ( is_admin() ) { ?>
<style>
	.bg-cta-box {
		border: 1px solid #ccc;
	}
</style>
<?php } ?>