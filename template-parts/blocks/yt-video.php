<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

	$className = 'yt-player';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

	$id = get_field('video_id');
	$thumb = get_field('featured_image');
	$size = 'full';
?>

<div class="<?php echo esc_attr($className); ?>">
	<div class="video-wrapper">
		<iframe class="yt-video" width="560" height="315" src="https://www.youtube.com/embed/<?= $id; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	</div>
	
	<div class="video-content" style="background-image: url(<?= $thumb; ?>);">
		<span class="yt-play">
			<svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 206 206"><g transform="translate(-477 -230)"><circle cx="103" cy="103" r="103" transform="translate(477 230)" fill="rgba(114,36,108,0.95)"/><path data-name="Path 8202" d="M559.463 295.305V370.7L620.54 333l-61.077-37.695z" fill="none" stroke="#ffa400" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/></g></svg>
		</span>
	</div>
</div>