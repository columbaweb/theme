<?php

/* Iframe Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'project-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
	$className = 'bir-frame';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

// Load values and assing defaults.
	$title = get_field('title');
	$url = get_field('iframe_url');
	$link = get_field('button');
	$id = get_field('id');
?>

<div class="<?= $className; ?>">
	<?php if ($title) { ?>
		<h3><?= $title; ?></h3>
	<?php } ?>

	<iframe <?php if ($id) { echo 'id="'.$id.'"'; } ?> src="<?= $url; ?>"></iframe>
	
	<?php 
		if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
	?>
		<a class="btn" href="<?= esc_url($link_url); ?>" target="<?= esc_attr($link_target); ?>">
			<?= esc_html($link_title); ?>
		</a>
	<?php endif; ?>
	
</div>