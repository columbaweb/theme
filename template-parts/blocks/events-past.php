<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}
	
	$className = 'events past';
	
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
?>

<div class="<?php echo esc_attr($className); ?>">
	
	<?php
		$today = date('Ymd');
		
		$args = array(
        	'post_type'  => 'events',
        	'posts_per_page'  => '-1',
        	'meta_key'			=> 'event_date',
        	'orderby'			=> 'meta_value',
        	'order' => 'DESC',

            'meta_query' => array(
        		array(
        	        'key'		=> 'event_date',
        	        'compare'	=> '<',
        	        'value'		=> $today,
        	    ),
            ),
        );

		$peQuery = new WP_Query( $args ); 
		global $post;
	?>
	
	<?php if ($peQuery->have_posts()): while ($peQuery->have_posts()): $peQuery->the_post(); ?>
		<div class="event">
			<?php
			    $today = date('Ymd');
			    $dateformatstring = "Ymd";
			    $dateformatstring2 = "d F Y";
			    
			    $unixtimestamp = strtotime(get_field('event_date', $post));
			    $calDate = date_i18n($dateformatstring, $unixtimestamp);
			    $calDateFront = date_i18n($dateformatstring2, $unixtimestamp);
			    
			    $unixtimestampend = strtotime(get_field('event_end_date', $post));
			    $calDateEnd = date_i18n($dateformatstring, $unixtimestampend);
			    $calDateFrontEnd = date_i18n($dateformatstring2, $unixtimestampend);
			    
			    $title=get_the_title();
			    $titleNoSpace = str_replace(' ', '%20', $title);
			?>
			
			<p class="event-title">
				<span class="meta">
					<?php echo $calDateFront;?>	
					<?php if(get_field('event_end_date', $post ) != ""): ?>
					    - <?php echo $calDateFrontEnd; ?>
					<?php endif; ?>
				</span>
				
				<strong><?php the_title(); ?></strong>
			</p>
		</div>		
	<?php endwhile; endif; wp_reset_query(); ?>

</div>

<?php if ( is_admin() ) { ?>
	<style type="text/css">
		.past .event {
			margin-bottom: 10px;
			padding: 15px;
			background: #fafafa;
		}
	</style>
<?php } ?>