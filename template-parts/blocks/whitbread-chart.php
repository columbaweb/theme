<?php

# ------------------------------------------
# GUTENBERG: WHITBREAD CHART
# ------------------------------------------

$id = 'chart-' . $block['id'];
if( !empty($block['anchor']) ) {
	$id = $block['anchor'];
}

$className = 'bir-block uk-german-chart';
if( !empty($block['className']) ) {
	$className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
	$className .= ' align' . $block['align'];
}

$uk_market = get_field('uk_market');

$uk_current_network = $uk_market['current_network'];
$uk_committed_pipeline = $uk_market['committed_pipeline'];
$uk_additional_potential = $uk_market['additional_potential'];

$total_uk_potential = $uk_current_network + $uk_committed_pipeline + $uk_additional_potential;

$german_market = get_field('german_market');

$german_current_network = $german_market['current_network'];
$german_committed_pipeline = $german_market['committed_pipeline'];
$german_additional_potential = $german_market['additional_potential'];

$total_german_potential = $german_current_network + $german_committed_pipeline + $german_additional_potential;

$total_potential = $total_uk_potential + $total_german_potential;

$total_uk_potential_percent = $total_uk_potential / $total_potential * 100;
$uk_additional_potential_percent = $uk_additional_potential / $total_uk_potential * 100;
$uk_committed_pipeline_percent = $uk_committed_pipeline / $total_uk_potential * 100;
$uk_current_network_percent = $uk_current_network / $total_uk_potential * 100;

$total_german_potential_percent = $total_german_potential / $total_potential * 100;
$german_additional_potential_percent = $german_additional_potential / $total_german_potential * 100;
$german_committed_pipeline_percent = $german_committed_pipeline / $total_german_potential * 100;
$german_current_network_percent = $german_current_network / $total_german_potential * 100;

?>

<div id="<?php echo $id; ?>" class="<?php echo esc_attr($className); ?>">

  <div class="top flex-end">

	    <div class="left col">
	      <div class="bars flex-end">
	        <div class="line flex-center" style="height: <?php echo $total_uk_potential_percent; ?>%">
	          <div class="arrow"></div>
	          <div class="text">
	            <div class="big">
	              ><?php echo $total_uk_potential; ?>k
	            </div>
	            <div class="small">
	              Total UK potential
	            </div>
	          </div>
	        </div>
	        <div class="boxes flex-center" style="height: <?php echo $total_uk_potential_percent; ?>%">
	          <div class="row flex-center" style="height: <?php echo $uk_additional_potential_percent; ?>%">
	            <div class="in-big">><?php echo $uk_additional_potential; ?>k</div>
	            <div class="in-small">Additional potential</div>
	          </div>
	          <div class="row flex-center" style="height: <?php echo $uk_committed_pipeline_percent; ?>%">
	            <div class="in-big">><?php echo $uk_committed_pipeline; ?>k</div>
	            <div class="in-small">Committed pipeline</div>
	          </div>
	          <div class="row flex-center" style="height: <?php echo $uk_current_network_percent; ?>%">
	            <div class="in-big">><?php echo $uk_current_network; ?>k</div>
	            <div class="in-small">Current network<sup>1</sup></div>
	          </div>
	        </div>
	      </div>
	    </div>

	    <div class="center col">
	      <div class="line flex-center">
	        <div class="arrow"></div>
	        <div class="text">
	          <div class="big">
	            ><?php echo $total_potential; ?>k
	          </div>
	          <div class="small">
	            Total potential
	          </div>
	        </div>
	      </div>
	    </div>

	    <div class="right col">
	      <div class="bars flex-end">
	        <div class="boxes flex-center" style="height: <?php echo $total_german_potential_percent; ?>%">
	          <div class="row flex-center" style="height: <?php echo $german_additional_potential_percent; ?>%">
	            <div class="in-big">><?php echo $german_additional_potential; ?>k</div>
	            <div class="in-small">Additional potential</div>
	          </div>
	          <div class="row flex-center" style="height: <?php echo $german_committed_pipeline_percent; ?>%">
	            <div class="in-big">><?php echo $german_committed_pipeline; ?>k</div>
	            <div class="in-small">Committed pipeline</div>
	          </div>
	          <div class="row flex-center" style="height: <?php echo $german_current_network_percent; ?>%">
	            <div class="in-big"><?php echo $german_current_network; ?>k</div>
	            <div class="in-small">Current network</div>
	          </div>
	        </div>
	        <div class="line flex-center" style="height: <?php echo $total_german_potential_percent; ?>%">
	          <div class="arrow"></div>
	          <div class="text">
	            <div class="big">
	              ><?php echo $total_german_potential; ?>k
	            </div>
	            <div class="small">
	              Total German potential
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>

  </div>

  <div class="bottom flex-end">

    <div class="left col">
      <div class="legend">
        UK MARKET
      </div>
    </div>

    <div class="center col">
      <div class="legend">
        TOTAL MARKET
      </div>
    </div>

    <div class="right col">
      <div class="legend">
        GERMAN MARKET
      </div>
    </div>
		
  </div>

</div>



<?php if ( is_admin() ) { ?>
	<style type="text/css">
    .flex-end {display: flex; justify-content: space-between; white-space: nowrap;}
    .flex-end > * {width: 33%;}
    .legend {font-weight: bold}
    .boxes {display: none;}
	</style>
<?php } ?>
