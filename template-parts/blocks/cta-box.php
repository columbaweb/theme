<?php

/* CTA Box Block Template */

$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$type = get_field('title_type');

$link = get_field('link');
$link_url = $link['url'];
$link_title = $link['title'];
$link_target = $link['target'] ? $link['target'] : '_self';
?>


<?php switch ($type) { case 'btn': ?>
	<div class="cta-box bx <?= esc_attr($classes); ?>">
		
		<?php if ( is_admin() ) { ?><p class="block-admin-label"><span>CTA block - click to add elements</span></p><?php } ?>
		
		<a class="title" href="<?= esc_url($link_url); ?>" target="<?= esc_attr($link_target); ?>">
			<?= esc_html($link_title); ?>
			<svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24.04" height="16.419"><g fill="none" stroke="#72246c" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.5"><path d="M21.702 8.073H1.25"/><path d="M15.83 14.652l6.442-6.442L15.83 1.77"/></g></svg>
				
		</a>
		
		<InnerBlocks />
	</div>

<?php break; default: ?>
	<a class="cta-box link <?= esc_attr($classes); ?>" href="<?= esc_url($link_url); ?>" target="<?= esc_attr($link_target); ?>">
		<?php if ( is_admin() ) { ?><p class="block-admin-label"><span>CTA block - click to add elements</span></p><?php } ?>
		
		<span>
			<?= esc_html($link_title); ?>
			<svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="14" height="12.243"><path d="M8.5 2.122l4 4-4 4" fill="none" stroke="#72246c" stroke-linecap="round" stroke-linejoin="round" stroke-width="3"/><path d="M11.5 6.121h-10" fill="#fd5c3f" stroke="#72246c" stroke-linecap="round" stroke-width="3"/>
		</span>
		
		<InnerBlocks />
		
	</a>
<?php break; } ?>


<?php if ( is_admin() ) { ?>
<script>
	jQuery( "a.cta-box" ).click(function( event ) {
		event.preventDefault();
	});
</script>
<?php } ?>
