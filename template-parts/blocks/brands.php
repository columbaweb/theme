<?php

/* Brands Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'project-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
	$className = 'our-brands';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

?>

<?php if( have_rows('brands') ): ?>

	<div class="<?php echo $className; ?>">
		<?php while( have_rows('brands') ): the_row(); ?>
		
		<a class="scroll" href="<?php the_sub_field('link'); ?>">
			<?php 
				$image = get_sub_field('logo');
				$size = 'full';
				if( $image ) {
				echo wp_get_attachment_image( $image, $size );
				}
			?>
		</a>
		
		<?php endwhile; ?> 
	</div>
	
<?php endif; ?>
