<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}
	
	$className = 'timeline';
	
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
?>

<?php if( have_rows('timeline') ): ?>
	<div class="<?php echo esc_attr($className); ?>">
	    <?php 
	    	while( have_rows('timeline') ): the_row(); 
	    ?>
			<div class="timeline-item">
				<h2 class="timeline-year"><?php the_sub_field('year'); ?></h2>
				
				<?php if ( get_sub_field('title')) { ?>
				<h3 class="timeline-title"><?php the_sub_field('title'); ?></h3>
				<?php } ?>
				
				<?php if ( ( get_sub_field('content')) || get_sub_field('featured_image') ) { ?>
				<div class="timeline-content">
					<?php 
						the_sub_field('content'); 
						
						$image = get_sub_field('featured_image');
						$size = 'full';
						if( $image ) {
						  echo wp_get_attachment_image( $image, $size );
						}
					?>
				</div>
				<?php } ?>
			</div>
	    <?php endwhile; ?> 
	</div>
<?php endif; ?>



<?php if ( !is_admin() ) { ?>

  

<?php } ?>
