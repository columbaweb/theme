<?php
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}
	
	$className = '';
	
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

	$category = get_field('case_study_category');
	$col = get_field('background_colour');
?>

<div class="case-studies-wrapper col-<?= $col; ?>">
	
	<?php
		$args = array(
			'post_type' => 'case-study',
			'posts_per_page' => -1,
			'order' => 'ASC',
			'tax_query' => array(
			    array(
			        'taxonomy' => 'case-study-category',
			        'field' => 'id',
					'terms'    => $category,
			    )
			)
		);
		$teamQuery = new WP_Query( $args ); 
		global $post;
	?>
	
	<?php if ($teamQuery->have_posts()): ?>
	
		<?php if (get_field('section_title')) { ?>
		<h3><?php the_field('section_title'); ?></h3>
		<?php } ?>
		
		<div class="cases-carousel-wrap <?= $className; ?>">
			<div class="cases-carousel-wrap-inner">
				<div class="cases-carousel">
		
					<?php while ($teamQuery->have_posts()): $teamQuery->the_post(); ?>
					
						<div class="slide has-border">
							
							<div class="slide-content">
							
								<h3 class="name"><?php the_title(); ?></h3>
								<?php if ( !is_admin() ) { ?>
									<?php the_content(); ?>
								<?php } ?>
						
							</div>
							
							<a class="slick-prev"></a>
							<a class="slick-next"></a>
				
						</div>
						
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; wp_reset_query(); ?>

</div>

