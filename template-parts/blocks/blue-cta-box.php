<?php

/* CTA Box Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'block-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'blue-cta-box';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
	
	$bg = get_field('background_image');
	$layout = get_field('image_position');
?>

<div class="<?= $className; ?> img-<?= $layout; ?>">
	<?php if ( is_admin() ) { ?><p class="block-admin-label"><span>CTA block - click to add elements</span></p><?php } ?>
	
	<span class="bg" style="background-image:  url(<?= $bg; ?>);"></span>
	
	<div class="wrap">
		<div class="inner">
			<InnerBlocks />
		</div>
	</div>
	
</div>