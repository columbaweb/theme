<?php

/* Brands Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'project-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
	$className = 'brand-title';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

?>

<div class="<?= $className; ?>">
	<?php if (get_field('title')) { ?>
	<h3 class="screen-reader-text"><?php the_field('title'); ?></h3>
	<?php } ?>
	
	<?php 
		$image = get_field('logo');
		$size = 'full';
		if( $image ) {
		  echo wp_get_attachment_image( $image, $size );
		}
	?>
	
	<ul class="brand-links">
	
		<?php if( have_rows('buttons') ): ?>

		
		    <?php while( have_rows('buttons') ): the_row(); ?>
				<li>
					<?php 
						$link = get_sub_field('link'); 
						if( $link ) {
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="btn style-<?php the_sub_field('style'); ?>" href="<?= esc_url($link_url); ?>" target="<?= esc_attr($link_target); ?>">
						<?= esc_html($link_title); ?>
						<svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="19.017" height="19.018"><g fill="none" stroke="#fff" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2.5"><path d="M17.25 1.768L6.453 12.565"/><path d="M6.417 3.587H1.25v14.18h14.18v-5.164" stroke-linejoin="round"/><path d="M17.249 8.122V1.768h-6.354" stroke-linejoin="round"/></g></svg>
					</a>
					<?php }  ?>
					
				</li>
		    <?php endwhile; ?> 
		
		<?php endif; ?>
		
		<?php if( have_rows('video_button') ): ?>
			<li>
				<?php while( have_rows('video_button') ): the_row(); ?>
				<a data-fancybox class="btn video style-<?php the_sub_field('style'); ?>" href="<?php the_sub_field('video_url'); ?>">
					Watch video
				</a>
		    <?php endwhile; ?>
			</li>
		<?php endif; ?>
	
	</ul>
	
</div>

<?php if ( is_admin() ) { ?>

<script>
	jQuery( ".brand-links a.btn" ).click(function( event ) {
		event.preventDefault();
	});
</script>

<?php } ?>

