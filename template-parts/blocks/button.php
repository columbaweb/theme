<?php

/* Button Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'project-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
	$className = 'btn';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}

// Load values and assing defaults.
	$link = get_field('link');
	$type = get_field('type');
	$style = get_field('style');
	$target = get_field('external');
?>

<?php 
	if( $link ): 
	$link_url = $link['url'];
	$link_title = $link['title'];
	$link_target = $link['target'] ? $link['target'] : '_self';
	
	if ($target) {
		$t = "ext";
		$icon = '<svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="19.017" height="19.018"><g data-name="Group 1331" fill="none" stroke="#72246c" stroke-linecap="round" stroke-miterlimit="10" stroke-width="2.5"><path data-name="Line 51" d="M17.25 1.768L6.453 12.565"/><path data-name="Path 7866" d="M6.417 3.587H1.25v14.18h14.18v-5.164" stroke-linejoin="round"/><path data-name="Path 7867" d="M17.249 8.122V1.768h-6.354" stroke-linejoin="round"/></g></svg>';
	} else {
		$t = "int";
		$icon = '<svg role="img" aria-hidden="true" width="13" height="11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 1.415l4 4-4 4" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M11 5.414H1" stroke="#fff" stroke-width="2" stroke-linecap="round"/></svg>';
	}
?>
	
	<?php if ( !is_admin() ) { ?>

		<?php
			switch ($type) {
				case 'file':
					echo '<a class="'.$className.' download" href="'.esc_url($link_url).'" target="'.esc_attr($link_target).'"><div class="btn"><span>'.esc_html($link_title).'</span><svg role="img" aria-hidden="true" width="16" height="12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.414 5.707l-5.707 5.707L8.293 10l4.293-4.293-4.293-4.293L9.707 0l5.707 5.707z" fill="#86B09B"/><path d="M0 4.707v2h13v-2H0z" fill="#86B09B"/></svg></svg></div></a>';
					break;

				default:
					echo '<a class="'.$className.' style-'.$style.' '.$t.'" href="'.esc_url($link_url).'" target="'.esc_attr($link_target).'">'.esc_html($link_title).' '.$icon.'</a>';
					break;
			}
		?>

	<?php } else { ?>
		<span class="<?= $className; ?> style-<?= $style; ?> <?= $t; ?>">
			<?= esc_html($link_title); ?>
		</span>
	<?php } ?>
	
<?php endif; ?>