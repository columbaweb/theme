<?php

/* Counter Block Template */

// Create id attribute allowing for custom "anchor" value.
	$id = 'project-' . $block['id'];
	if( !empty($block['anchor']) ) {
	    $id = $block['anchor'];
	}

// Create class attribute allowing for custom "className" and "align" values.
	$className = 'counter-container';
	if( !empty($block['className']) ) {
	    $className .= ' ' . $block['className'];
	}
	if( !empty($block['align']) ) {
	    $className .= ' align' . $block['align'];
	}
?>

<?php if( have_rows('counter') ): ?>

	<div class="<?php echo $className; ?> ">
		
	    <?php while( have_rows('counter') ): the_row(); ?>
			<div class="counter <?php the_sub_field('additional_class_name'); ?> <?php if (get_sub_field('circle')) { echo "has-circle"; } ?>">
				
				<?php if (get_sub_field('circle')) { echo '<div class="circle">'; } ?>

				<?php if( get_sub_field('title_4') ) { ?>
					<p class="title top-title <?php the_sub_field('title_colour'); ?>"><?php the_sub_field('title_4') ?></p>
				<?php } ?>
				
				<?php if ( 
					(get_sub_field('prefix')) || 
					(get_sub_field('counter_number')) || 
					(get_sub_field('decimal_number')) || 
					(get_sub_field('suffix')) ) 
				{ ?>
				
				<p class="number <?php the_sub_field('number_colour'); ?>">
					
					<?php if( get_sub_field('prefix') ) { ?>
						<span class="pref"><?php the_sub_field('prefix'); ?></span>
					<?php } ?>
					
					<?php if( get_sub_field('counter_number') ) { ?>
						<span class="count" data-from="0" data-to="<?php the_sub_field('counter_number'); ?>"><?php the_sub_field('counter_number'); ?></span>
					<?php } ?>
					
					<?php if( get_sub_field('decimal_number') ) { ?>
						<span class="dec"><?php the_sub_field('decimal_number'); ?></span>
					<?php } ?>
					
					<?php if( get_sub_field('suffix') ) { ?>
						<span class="suf"><?php echo the_sub_field('suffix'); ?></span>
					<?php } ?>
				</p>
				
				<?php } ?>
			
				<?php if( get_sub_field('title_2') ) { ?>
					<p class="has-small-font-size"><?php the_sub_field('title_2') ?></p>
				<?php } ?>

				<?php if( get_sub_field('title_3') ) { ?>
					<p class="title  <?php the_sub_field('title_colour'); ?>"><?php the_sub_field('title_3') ?></p>
				<?php } ?>
				
				<?php if( get_sub_field('footnote') ) { ?>
					<p class="footnote"><?php the_sub_field('footnote') ?></p>
				<?php } ?>
				
				<?php if (get_sub_field('circle')) { echo '</div>'; } ?>
	
			</div>
	
	    <?php endwhile; ?> 
		
		<?php if (get_field('caption')) { ?>
			<p class="counter-caption"><?php the_field('caption'); ?></p>
		<?php } ?>
		
	</div>
	
<?php endif; ?>