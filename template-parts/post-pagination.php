<?php
	the_posts_pagination( 
		array(
	        'mid_size'  => 2,
	        'prev_text' => __( 'Prev', 'bir' ),
	        'next_text' => __( 'Next', 'bir' ),
		) 
	);
?>