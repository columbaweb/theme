<?php get_header(); ?>

	<div class="wrap <?php echo get_theme_mod('bir_archive_sidebar'); ?>">
		
		<article id="content">
			<div class="inner">
			
				<h2>
					<?php 
						if ( is_day() ) : 
							printf('<span>%s</span>', get_the_date() );
						elseif ( is_month() ) : 
							printf('<span>%s</span>', get_the_date('F Y') );
						elseif ( is_year() ) :
							printf('<span>%s</span>', get_the_date('Y') );
						elseif ( is_tag() ) :
							echo __( 'Tagged: ', 'ir' );
							single_tag_title();
						elseif ( is_category() ) :
							single_cat_title();
						endif; 
					?>
				</h2>
				
				<?php 
			 		the_archive_description( '<div class="intro">', '</div>' ); 
			 		$count=0; if (have_posts()): 
			 	?>
			 	
		 		<div class="media latest-posts col">
	                <?php 
	                	while (have_posts()): the_post(); $count++;
	                		if (( $count == 1) || ( $count == 2) || ( $count == 3) || ( $count == 4) || ( $count == 5)) {
								include( get_stylesheet_directory() . '/template-parts/content-excerpt.php' ); 
							} else {  
								include( get_stylesheet_directory() . '/template-parts/content-excerpt-small.php' ); 
							} 
	                	endwhile;
	                ?>
		 		</div>
	
	            <?php get_template_part('template-parts/post', 'pagination'); ?>
	
	            <?php else : 
	            	get_template_part( 'template-parts/content', 'none' );
	            endif; ?>
			
			</div>
		</article>
		
		<?php if( ((get_theme_mod('bir_archive_sidebar')) != "full-width") ) { ?>
		<aside id="sidebar" class="widget-area">
			<h2 class="side-title is-style-border"><a href="<?php bloginfo('url'); ?>/media/">Media</a></h2>
			<?php 
				wp_nav_menu( array(
				    'menu'           => 'Press release',
				) );
			?>
			<?php dynamic_sidebar('media'); ?>
		</aside>
		<?php } ?>

	</div>
	
<?php get_footer(); ?>