// GTB block modifications

// ------------------------------------------
// paragraph styles
// ------------------------------------------
wp.blocks.registerBlockStyle( 'core/paragraph', {
    name: 'intro',
    label: 'Intro',
} );
wp.blocks.registerBlockStyle( 'core/paragraph', {
    name: 'lead',
    label: 'Page lead',
} );
wp.blocks.registerBlockStyle( 'core/paragraph', {
    name: 'small',
    label: 'Small',
} );


// ------------------------------------------
// columns styles
// ------------------------------------------
wp.blocks.registerBlockStyle( 'core/columns', {
    name: 'border-sep',
    label: 'Border separator',
} );
wp.blocks.registerBlockStyle( 'core/columns', {
    name: 'lg-gutter',
    label: 'Large Gutter',
} );


// ------------------------------------------
// column styles
// ------------------------------------------
wp.blocks.registerBlockStyle( 'core/column', {
    name: 'bx',
    label: 'Boxed - grey border',
} );
wp.blocks.registerBlockStyle( 'core/column', {
    name: 'bx-white',
    label: 'Boxed - white border',
} );


// ------------------------------------------
// table styles
// ------------------------------------------
wp.blocks.registerBlockStyle( 'core/table', {
    name: 'cardtable',
    label: 'Mobile: Cardtable',
} );

wp.blocks.registerBlockStyle( 'core/table', {
    name: 'stacktable',
    label: 'Mobile: Stack Table',
} );

wp.blocks.registerBlockStyle( 'core/table', {
    name: 'stackcolumns',
    label: 'Mobile: Stack Columns',
} );


// ------------------------------------------
// list styles
// ------------------------------------------
wp.blocks.registerBlockStyle( 'core/list', {
    name: 'branded',
    label: 'Branded',
	isDefault: false,
} );