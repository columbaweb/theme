(function($) {

// ===============================================
// MENU
// ===============================================
$(function() {
    $('#nav-expander').css('cursor','pointer');
    $('#nav-expander').on('click',function(e){
        e.preventDefault();
        $('body').toggleClass('nav-expanded');
        $('.nav-expander').toggleClass('is-active');
    });
}); 


// ===============================================
// MOBILE SUBMENU
// ===============================================
$('.sitenav .sub-toggle').click(function() {
    $(this).toggleClass('open');
    $(this).next('.sub-menu').slideToggle('fast');
});


// ===============================================
// STICKY HEADER / SIDEBAR
// ===============================================
jQuery(document).ready(function($) {
    var stickyHead = function() {
        $('.header.sticky').toggleClass('on', $(document).scrollTop() > 35);
    }
    stickyHead();
    $(window).on('scroll touchmove', stickyHead);
});
 

// ===============================================
// FLOATING HEADER
// ===============================================
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.header.floating').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    if (st > lastScrollTop && st > navbarHeight){
        $('header').removeClass('nav-down').addClass('nav-up');
    } else {
        if(st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up').addClass('nav-down');
        }
    }
    
    lastScrollTop = st;
}


// -----------------------------------------------------------------------------
// tabs - add hast to the URL
// -----------------------------------------------------------------------------
var url = document.URL;
var hash = url.substring(url.indexOf('#'));

$(".kt-tabs-wrap").find("li > a").each(function(key, val) {
    if (hash == $(val).attr('href')) {
        $(val).click();
    }
    
    $(val).click(function(ky, vl) {
		var scrollTop = $(window).scrollTop();
        location.hash = $(this).attr('href');
		$(window).scrollTop(scrollTop);
		
    });
	
	$(".kt-tabs-wrap li > a").click(function(e) {
		e.preventDefault();
	});
});

 
// -----------------------------------------------------------------------------
// b2t
// -----------------------------------------------------------------------------
jQuery(document).ready(function($){
    var offset = 300,
        offset_opacity = 1200,
        scroll_top_duration = 700,
        $back_to_top = $('.cd-top');
 
//hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) {
            $back_to_top.addClass('cd-fade-out');
        }     });
 
//smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
            scrollTop: 0 ,
             }, scroll_top_duration
        );
    });
});
 
 
// -----------------------------------------------------------------------------
// slider
// -----------------------------------------------------------------------------
$('.slider').slick({
	dots: true,
	arrows: true,
	prevArrow : '<a class="slick-prev"><svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="41.012" height="41.012"><path d="M20.506 4.243L4.243 20.506 20.506 36.77" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/></svg></a>',
 	nextArrow : '<a class="slick-next"><svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="41.012" height="41.012"><path d="M20.507 4.243L36.77 20.506 20.507 36.769" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/></svg></a>',
	infinite: true,
	fade: true,
	cssEase: 'linear',
	adaptiveHeight: false,
	autoplay: true,
	autoplaySpeed: 7500,
	speed: 1100,
});


// -----------------------------------------------------------------------------
// case studies study carousel
// -----------------------------------------------------------------------------
jQuery( ".cases-carousel-wrap" ).each(function() {
	$( this ).find('.cases-carousel').slick({
		infinite: true,
		prevArrow: $( this ).find('.slick-prev'),
		nextArrow: $( this ).find('.slick-next'),
		dots: false,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 9000,
		speed: 650,
	});

});


// -----------------------------------------------------------------------------
// images study carousel
// -----------------------------------------------------------------------------
jQuery( ".img-carousel-wrap" ).each(function() {
	$( this ).find('.img-carousel').slick({
		dots: false,
		arrows: true,
		prevArrow: $( this ).find('.slick-prev'),
		nextArrow: $( this ).find('.slick-next'),
		centerMode: true,
		centerPadding: '0px',
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: true,
		cssEase: 'linear',
		adaptiveHeight: false,
		autoplay: true,
		autoplaySpeed: 9000,
		speed: 650,
		
		responsive: [
			{
			  breakpoint: 1290,
			  settings: {
			  	slidesToShow: 1,
				centerPadding: 'calc(29vw + 100px)',
			  }
			},
			
			{
			  breakpoint: 1200,
			  settings: {
			  	slidesToShow: 1,
				centerPadding: 'calc(28vw + 70px)',
			  }
			},
			{
			  breakpoint: 1160,
			  settings: {
			  	slidesToShow: 1,
				centerPadding: 'calc(29.5vw + 70px)',
			  }
			},
			{
			  breakpoint: 1100,
			  settings: {
			  	slidesToShow: 1,
				centerPadding: 'calc(31vw + 70px)',
			  }
			},
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 1,
			    centerPadding: '5vw',
			  }
			}
		]
		
	});
});


// -----------------------------------------------------------------------------
// brands slider
// -----------------------------------------------------------------------------
let dotTitles = []
$('.brands-carousel .slide').each(function(i) {
	dotTitles.push($(this).data('title'));
});

const paging = function(slick,index) {
  return '<span>' + dotTitles[index] + '</span>';
}

$(".brands-carousel").slick({
	dots: true,
    arrows: false,
    infinite: true,
	fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 2500,
	speed: 500,
	dots: true,
    customPaging: paging,
    appendDots: $("#carouselPager"),
    dotsClass: "carouselDots"
});


// -----------------------------------------------------------------------------
// team toggle
// -----------------------------------------------------------------------------
$(function() {
    $('.team-head').click(function(){
        $('.open').removeClass('open');
		$('.fade').removeClass('fade');
		
        if(false == $(this).next().is(':visible')) {
            $('.team-bio').slideUp('normal');
            $(this).addClass('open');
			$(this).parent().toggleClass('fade');
        }
        $(this).next().slideToggle('normal');
    });
});


// -----------------------------------------------------------------------------
// number animation
// -----------------------------------------------------------------------------
$( '.counter' ).waypoint( function () {
	$(this.element).addClass("in-view");
	
	$( '.count' ).countTo( {
		speed: 1000,
		formatter: function ( value, options ) {
			value = value.toFixed( options.decimals );
			value = value.replace( /\B(?=(\d{3})+(?!\d))/g, ',' );
			return value;
		}
	} );
	
}, { offset: '100%' } );


// -----------------------------------------------------------------------------
// IFRAME RESIZE
// -----------------------------------------------------------------------------
window.addEventListener('message', function(event) {
	var frames = document.getElementsByTagName('iframe');
    for (var i = 0; i < frames.length; i++) {
        if (frames[i].contentWindow === event.source) {
            $(frames[i]).height(event.data);
            break;
        }
    }
});

function resize() {
  window.addEventListener('message', function(event) {
      var frames = document.getElementsByTagName('iframe');
      for (var i = 0; i < frames.length; i++) {
          if (frames[i].contentWindow === event.source) {
              $(frames[i]).height(event.data);
              break;
          }
      }
  });
}

$('.vc_tta-tab').click(function() {
  var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
  if( isSafari ) {
      resize();
  }
});


// -----------------------------------------------------------------------------
// STRETCH FULL ROW
// -----------------------------------------------------------------------------
jQuery("document").ready(function($){
    var viewportWidth = document.querySelectorAll(".side-left .alignfull");
    var setMargin = function setMargin() {
    requestAnimationFrame(function () {
        [].forEach.call(viewportWidth, function (e) {
        e.style.marginLeft = "";
        e.style.marginLeft = -e.offsetLeft + "px";
        e.style.width = document.body.clientWidth + "px";
        });
    });
    };
    setMargin();
    window.onresize = setMargin;
});


// -----------------------------------------------------------------------------
// play youtube video on click
// -----------------------------------------------------------------------------
jQuery(".yt-play").click(function () {
	$(this).parent().parent().find(".video-content").hide();
	$(this).parent().parent().find(".yt-video")[0].src += "?autoplay=1";
	setTimeout(function(){ jQuery(".yt-video").show(); }, 250);
});


// -----------------------------------------------------------------------------
// tables
// -----------------------------------------------------------------------------
$('.is-style-stacktable table').stacktable({myClass:'mob-table'});
$('.is-style-stackcolumns table').stackcolumns({myClass:'mob-table cols'});
$('.is-style-cardtable table').cardtable({myClass:'mob-table'});


})( jQuery );