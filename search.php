<?php get_header(); ?>

	<div class="wrap full-width">
		
	    <article id="content">
			
			<div class="inner">
			
				<h1 class="page-title"><?php echo __( 'Search results for', 'bir' ); ?> <?php the_search_query(); ?></h1>
		
		        <?php if (have_posts()) : ?>
		            <?php if ( have_posts() ) :  ?>
					<div class="search-results">
						<?php while ( have_posts() ) : the_post(); ?>
					
		                <a class="search-result" href="<?php the_permalink(); ?>">
							<h5><?php the_title(); ?></h5>
							<span class="btn style-link-col">View more <svg role="img" aria-hidden="true" width="13" height="11" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8 1.415l4 4-4 4" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path><path d="M11 5.414H1" stroke="#fff" stroke-width="2" stroke-linecap="round"></path></svg></span>		
		                </a>
						
			            <?php
			                endwhile;
							get_template_part('template-parts/post', 'pagination'); 
			            ?>
					</div>
					<?php endif; ?>
		
		        <?php else : ?>
					<p>Nothing found</p>
		        <?php endif; ?>
				
			</div>	
	
	    </article>

	</div>

<?php get_footer(); ?>