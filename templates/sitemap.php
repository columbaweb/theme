<?php /* Template Name: Sitemap */  ?>

<?php get_header(); ?>

	<?php get_template_part('template-parts/content', 'pagehead'); ?>
	
	<div class="wrap <?php the_field('sidebar_position'); ?>">
		
		<article id="content">
			
			<?php 
				if (!is_front_page()) { 
					$type = get_field('page_title');
					switch ($type) {
						case 'default':
							echo '<h1 class="page-title">'.get_the_title().'</h1>';
						break;
						case 'custom':
							echo '<h1 class="page-title">'.get_field('custom_page_title').'</h1>';
						break;
					}
				} 
			?>
			
			<?php
				if ( have_posts() ) { while ( have_posts() ) { the_post();
					the_content();
				} }
			?>
			
			<div class="sitemap-container">
				<div class="col">
					<h3>Pages</h3>
					<ul><?php wp_list_pages("title_li=" ); ?></ul>
					
					
					<h3>Categories</h3>
					<ul><?php wp_list_cats('sort_column=name&optioncount=1&hierarchical=0&feed=RSS'); ?></ul>
					
					<h2>Archives</h2>
	                <ul>
	                    <?php wp_get_archives('type=monthly&show_post_count=true'); ?>
	                </ul>
				</div>
				
				<div class="col">
					<h3>Media</h3>
	                <ul><?php $archive_query = new WP_Query('showposts=1000&cat=-8');
	                        while ($archive_query->have_posts()) : $archive_query->the_post(); ?>
	                            <li>
	                                <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a>
	                            </li>
	                        <?php endwhile; ?>
	                </ul>
				</div>
			</div>
			
		</article>
		
	</div>
	
<?php get_footer(); ?>