<aside id="sidebar" class="widget-area">
	
	<?php 

// MENUS	
		if(!is_archive()) {
			get_template_part( 'template-parts/sidebar', 'title' ); 			// display page title
			get_template_part( 'template-parts/sidebar', 'menu' );  			// child pages menu
		}

// --- ACF widget area to display custom sidebar
		if( get_field('sidebar_position') == 'side-left' ) {
	   		if (get_field('sidebar_name')) { ?>
	   		
	   			<?php 
	   				$sidebar = get_field('sidebar_name'); 
	   				dynamic_sidebar($sidebar);
	   			?>
	   		
	   		<?php } 
		}
	?>

</aside>
