<?php


# ------------------------------------------
# Attached document listing
# ------------------------------------------
class filecta_widget extends WP_Widget {

function __construct() {
    parent::__construct(
        'filecta_widget',
        __('File CTA', 'filecta_widget_domain'),

        array( 'description' => __( 'File CTA', 'filecta_widget_domain' ), )
    );
}


// front
public function widget( $args, $instance ) {
	extract( $args );
	$widget_id = $this->id; 
	
	$title = apply_filters( 'widget_title', $instance['title'] );
	
    echo $args['before_widget'];
    global $post;


// ACF variables
	$fileURL = get_field('widget_file', 'widget_' . $widget_id); 
	$featImg = get_field('featured_image', 'widget_' . $widget_id);
	$size = 'large';
	$featImgWidth = $featImg['sizes'][ $size . '-width' ];
	$featImgHeight = $featImg['sizes'][ $size . '-height' ];
?>

<?php echo '<div class="file-cta">'; ?>

<?php 
	echo '<a class="file-cta" href='.$fileURL.' target="_blank">'; 
	echo '<img src="'.$featImg["url"].'" width="'.$featImgWidth.'" height="'.$featImgHeight.'" alt="download file" />'; 
	echo '<span class="title">';
	echo '<svg role="img" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="25"><g fill="none" fill-rule="evenodd" transform="translate(0 .149171)"><circle cx="12" cy="12" r="12" fill="#C6505E"/><path stroke="#FFF" stroke-linecap="square" stroke-width="3" d="M12 6v4.8"/><path stroke="#FFF" stroke-width="2.4" d="M7.5 9.38408972l4.4575495 4.41495478L16.5 9.3"/><path stroke="#FFF" stroke-linecap="square" stroke-width="1.8" d="M16.2 17.7H7.5"/></g></svg>';
	echo $title;
	echo '</span>';
	echo '</a>';
	echo '</div>'; 
?>

<?php
    echo $args['after_widget'];
}


// back
public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
        $title = $instance[ 'title' ];
    }
    else {
        $title = __( 'New title', 'filecta_widget_domain' );
    }
    ?>
    <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>
    
    <?php
    }
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}

// Register and load the widget
function filecta_load_widget() {
	register_widget( 'filecta_widget' );
}
add_action( 'widgets_init', 'filecta_load_widget' );

?>
