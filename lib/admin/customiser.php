<?php

# ------------------------------------------
# CUSTOMIZER
# ------------------------------------------
function bir_customizer( $wp_customize ) {


# PANEL : THEME SETTINGS
# ------------------------------------------
$wp_customize->add_panel(
    'bir_panel_themesettings',
    array(
        'priority'       => 30,
        'capability'     => 'edit_theme_options',
        'theme_supports' => '',
        'title'          => 'Theme Settings',
        'description'    => '',
    )
);


# CONTROL : THEME SETTINGS > HEADER
# ------------------------------------------
$wp_customize->add_section( 'bir_section_header', array(
    'priority'       => 10,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Header',
    'description'    => '',
    'panel'  => 'bir_panel_themesettings',
) );

// header type
$wp_customize->add_setting(
    'bir_header_type',
    array(
        'default' => 'reg',
        'sanitize_callback' => 'bir_customizer_sanitize_radio',
    )
);
$wp_customize->add_control(
    'bir_header_type',
    array(
        'type' => 'radio',
        'label' => __('Header type', 'ir'),
        'section' => 'bir_section_header',
        'choices' => array(
        'reg' => __( 'Default' ),
        'sticky' => __( 'Sticky' ),
        'floating' => __( 'Floating' ),
      ),
    )
);


# CONTROL : THEME SETTINGS > FOOTER
# ------------------------------------------
$wp_customize->add_section( 'bir_section_footer', array(
    'priority'       => 10,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Footer',
    'description'    => '',
    'panel'  => 'bir_panel_themesettings',
) );

// copyright statement
 $wp_customize->add_setting (
    'footer_copy',
    array(
        'default' => '',
    )
);
$wp_customize->add_control (
    'footer_copy',
    array(
        'label' => 'Copyright statement',
        'section' => 'bir_section_footer',
        'type' => 'textarea',
    )
);

// B2T
 $wp_customize->add_setting (
    'show_credit',
    array(
        'default' => 0,
    )
);
$wp_customize->add_control (
    'show_credit',
    array(
        'label'     => 'Display credit',
        'section'   => 'bir_section_footer',
        'type'      => 'checkbox',
    )
);

// B2T
 $wp_customize->add_setting (
    'show_backtotop',
    array(
        'default' => 0,
    )
);
$wp_customize->add_control (
    'show_backtotop',
    array(
        'label'     => 'Back to top',
        'section'   => 'bir_section_footer',
        'type'      => 'checkbox',
    )
);


# CONTROL : THEME SETTINGS > SIDEBARS
# ------------------------------------------
$wp_customize->add_section( 'bir_section_sidebars', array(
    'priority'       => 10,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Sidebars',
    'description'    => '',
    'panel'  => 'bir_panel_themesettings',
) );

// post sidebars
$wp_customize->add_setting(
    'bir_post_sidebar',
    array(
        'default' => 'sidebar__left',
        'sanitize_callback' => 'bir_customizer_sanitize_radio',
    )
);
$wp_customize->add_control(
    'bir_post_sidebar',
    array(
        'type' => 'radio',
        'label' => __('Post sidebars', 'ir'),
        'description' => __( 'Sidebar location on posts' ),
        'section' => 'bir_section_sidebars',
        'choices' => array(
        'sidebar__left' => __( 'Left sidebar' ),
        'sidebar__right' => __( 'Right sidebar' ),
        'full-width' => __( 'No sidebar' ),
      ),
    )
);

// archive sidebars
$wp_customize->add_setting(
    'bir_archive_sidebar',
    array(
        'default' => 'side-left',
        'sanitize_callback' => 'bir_customizer_sanitize_radio',
    )
);
$wp_customize->add_control(
    'bir_archive_sidebar',
    array(
        'type' => 'radio',
        'label' => __('Category sidebars', 'ir'),
        'description' => __( 'Sidebar location on categories' ),
        'section' => 'bir_section_sidebars',
        'choices' => array(
        'side-left' => __( 'Left sidebar' ),
        'side-right' => __( 'Right sidebar' ),
        'full-width' => __( 'No sidebar' ),
      ),
    )
);

// search sidebars
$wp_customize->add_setting(
    'bir_search_sidebar',
    array(
        'default' => 'side-left',
        'sanitize_callback' => 'bir_customizer_sanitize_radio',
    )
);
$wp_customize->add_control(
    'bir_search_sidebar',
    array(
        'type' => 'radio',
        'label' => __('Search page sidebars', 'ir'),
        'description' => __( 'Sidebar location on search results page' ),
        'section' => 'bir_section_sidebars',
        'choices' => array(
        'side-left' => __( 'Left sidebar' ),
        'side-right' => __( 'Right sidebar' ),
        'full-width' => __( 'No sidebar' ),
      ),
    )
);

function bir_customizer_sanitize_radio( $input, $setting ) {
    $input = sanitize_key( $input );
    $choices = $setting->manager->get_control( $setting->id )->choices;
    return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

# CONTROL : THEME SETTINGS > POST TYPES
# ------------------------------------------
$wp_customize->add_section( 'bir_section_modules', array(
    'priority'       => 30,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Post Types',
    'description'    => '<strong>Activate post types</strong>',
    'panel'  => 'bir_panel_themesettings',
) );


# team 
$wp_customize->add_setting('bir_add_team', array(
    'default'    => false,
));
$wp_customize->add_control(
    new WP_Customize_Control(
        $wp_customize,
        'bir_add_team',
        array(
            'label'     => __('Team', 'ir'),
            'section'   => 'bir_section_modules',
            'settings'  => 'bir_add_team',
            'type'      => 'checkbox',
        )
    )
);

# careers
$wp_customize->add_setting('bir_add_jobs', array(
    'default'    => false,
));
$wp_customize->add_control(
    new WP_Customize_Control(
        $wp_customize,
        'bir_add_jobs',
        array(
            'label'     => __('Careers', 'ir'),
            'section'   => 'bir_section_modules',
            'settings'  => 'bir_add_jobs',
            'type'      => 'checkbox',
        )
    )
);

# documents
$wp_customize->add_setting('bir_add_docs', array(
    'default'    => false,
));
$wp_customize->add_control(
    new WP_Customize_Control(
        $wp_customize,
        'bir_add_docs',
        array(
            'label'     => __('Documents', 'ir'),
            'section'   => 'bir_section_modules',
            'settings'  => 'bir_add_docs',
            'type'      => 'checkbox',
        )
    )
);

# events
$wp_customize->add_setting('bir_add_events', array(
    'default'    => false,
));
$wp_customize->add_control(
    new WP_Customize_Control(
        $wp_customize,
        'bir_add_calendar',
        array(
            'label'     => __('Events', 'ir'),
            'section'   => 'bir_section_modules',
            'settings'  => 'bir_add_events',
            'type'      => 'checkbox',
        )
    )
);

}
add_action( 'customize_register', 'bir_customizer' );