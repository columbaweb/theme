<?php

# ------------------------------------------
# X-Frame-Options
# ------------------------------------------
add_action( 'init', 'bir_handle_preflight' );
function bir_handle_preflight() {
    header("Access-Control-Allow-Origin: " . get_http_origin());
    header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");
    header("Access-Control-Allow-Credentials: true");

    if ( 'OPTIONS' == $_SERVER['REQUEST_METHOD'] ) {
        status_header(200);
        exit();
    }
}


# ------------------------------------------
# REMOVE ob_end_flush
# ------------------------------------------
remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );


# ------------------------------------------
# CLEAN UP HEAD
# ------------------------------------------
function bir_remove_api () {
	remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
}
add_action( 'after_setup_theme', 'bir_remove_api' );


# ------------------------------------------
# CLEAN UP W3C
# ------------------------------------------
add_action( 'template_redirect', function(){
    ob_start( function( $buffer ){
        $buffer = str_replace( array( 'type="text/javascript"', "type='text/javascript'" ), '', $buffer );
        $buffer = str_replace( array( 'type="text/css"', "type='text/css'" ), '', $buffer );
        $buffer = str_replace( array( 'frameborder="0"', "frameborder='0'" ), '', $buffer );
        $buffer = str_replace( array( 'scrolling="no"', "scrolling='no'" ), '', $buffer );
        return $buffer;
    });
});


# ------------------------------------------
# DISABLE EMOJI
# ------------------------------------------
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );


# ------------------------------------------
# REMOVE WP VERSION
# ------------------------------------------
add_filter('the_generator', '__return_false');


# ------------------------------------------
# DISABLE PING BACK SCANNER AND XMLRPC
# ------------------------------------------
add_filter( 'wp_xmlrpc_server_class', '__return_false' );
add_filter('xmlrpc_enabled', '__return_false');


# ------------------------------------------
# REMOVE UNNECESSARY HEADER INFORMATION
# ------------------------------------------
function bir_remove_header_info() {
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action( 'wp_head', 'feed_links', 2 );
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'start_post_rel_link');
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head',10,0); // for WordPress >= 3.0
}
add_action('init', 'bir_remove_header_info');


# ------------------------------------------
# DISABLE RSS FEEDS
# ------------------------------------------
function bir_disable_feed() {
    wp_die( __('No feed available, please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}
add_action('do_feed', 'bir_disable_feed', 1);
add_action('do_feed_rdf', 'bir_disable_feed', 1);
add_action('do_feed_rss', 'bir_disable_feed', 1);
add_action('do_feed_rss2', 'bir_disable_feed', 1);
add_action('do_feed_atom', 'bir_disable_feed', 1);
add_action('do_feed_rss2_comments', 'bir_disable_feed', 1);
add_action('do_feed_atom_comments', 'bir_disable_feed', 1);


# ------------------------------------------
# DISABLE JSON REST API
# ------------------------------------------
//add_filter('json_enabled', '__return_false');
//add_filter('json_jsonp_enabled', '__return_false');


# ------------------------------------------
# REMOVE XPINGBACK
# ------------------------------------------
function bir_remove_x_pingback($headers) {
    unset($headers['X-Pingback']);
    return $headers;
}
add_filter('wp_headers', 'bir_remove_x_pingback');


# ------------------------------------------
# CHANGE AUTHOR URL SLUG TO NICKNAME
# http://wordpress.stackexchange.com/questions/5742/change-the-author-slug-from-username-to-nickname
# ------------------------------------------
function wpse5742_request( $query_vars ) {
    if ( array_key_exists( 'author_name', $query_vars ) ) {
        global $wpdb;
        $author_id = $wpdb->get_var( $wpdb->prepare( "SELECT user_id FROM {$wpdb->usermeta} WHERE meta_key='nickname' AND meta_value = %s", $query_vars['author_name'] ) );
        if ( $author_id ) {
            $query_vars['author'] = $author_id;
            unset( $query_vars['author_name'] );
        }
    }

    return $query_vars;
}
add_filter( 'request', 'wpse5742_request' );

function wpse5742_author_link( $link, $author_id, $author_nicename ) {
    $author_nickname = get_user_meta( $author_id, 'nickname', true );
    if ( $author_nickname ) {
        $link = str_replace( $author_nicename, $author_nickname, $link );
    }
    return $link;
}
add_filter( 'author_link', 'wpse5742_author_link', 10, 3 );

function wpse5742_set_user_nicename_to_nickname( &$errors, $update, &$user ) {
    if ( ! empty( $user->nickname ) ) {
        $user->user_nicename = sanitize_title( $user->nickname, $user->display_name );
    }
}
add_action( 'user_profile_update_errors', 'wpse5742_set_user_nicename_to_nickname', 10, 3 );


# ------------------------------------------
# DISABLE AUTHOR PAGE
# https://perishablepress.com/stop-user-enumeration-wordpress/
# ------------------------------------------
if (!is_admin()) {
    // default URL format
    if (preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'])) die(); add_filter('redirect_canonical', 'shapeSpace_check_enum', 10, 2);
}
function shapeSpace_check_enum($redirect, $request) {
    // permalink URL format
    if (preg_match('/\?author=([0-9]*)(\/*)/i', $request)) die(); else return $redirect;
}


# ------------------------------------------
# DISABLE COMMENTS
# ------------------------------------------
function bir_disable_comments_status() {
    return false;
}
add_filter('comments_open', 'bir_disable_comments_status', 20, 2);
add_filter('pings_open', 'bir_disable_comments_status', 20, 2);


# ------------------------------------------
# DISABLE YOAST SLACK
# ------------------------------------------
function bir_disable_slack_enhancements() {
  if ( 'post' !== get_post_type() ) {
    add_filter( 'wpseo_output_enhanced_slack_data', '__return_false' );
  }
}
add_action( 'wp', 'bir_disable_slack_enhancements' );


# ------------------------------------------
# IMAGE MAX WIDTH
# ------------------------------------------
function bir_big_image_size_threshold( $threshold, $imagesize, $file, $attachment_id ) {
	return 1650;
}
add_filter( 'big_image_size_threshold', 'bir_big_image_size_threshold', 10, 4 );


# ------------------------------------------
# SET CONTENT WIDTH
# ------------------------------------------
global $content_width;
if ( ! isset( $content_width ) ) {
	$content_width = 1440;
}


# ------------------------------------------
# DUPLICATE PAGES AND POSTS
# ------------------------------------------
function bir_duplicate_post_as_draft(){
    global $wpdb;
    if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'bir_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
        wp_die('No post to duplicate has been supplied!');
    }
 
    if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
        return;
 
    $post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
    $post = get_post( $post_id );
    $current_user = wp_get_current_user();
    $new_post_author = $current_user->ID;

    if (isset( $post ) && $post != null) {

        $args = array(
            'comment_status' => $post->comment_status,
            'ping_status'    => $post->ping_status,
            'post_author'    => $new_post_author,
            'post_content'   => $post->post_content,
            'post_excerpt'   => $post->post_excerpt,
            'post_name'      => $post->post_name,
            'post_parent'    => $post->post_parent,
            'post_password'  => $post->post_password,
            'post_status'    => 'draft',
            'post_title'     => $post->post_title,
            'post_type'      => $post->post_type,
            'to_ping'        => $post->to_ping,
            'menu_order'     => $post->menu_order
        );
 
        $new_post_id = wp_insert_post( $args );
 
        $taxonomies = get_object_taxonomies($post->post_type);
        foreach ($taxonomies as $taxonomy) {
            $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
            wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
        }
 
        $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
        if (count($post_meta_infos)!=0) {
            $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
            foreach ($post_meta_infos as $meta_info) {
                $meta_key = $meta_info->meta_key;
                if( $meta_key == '_wp_old_slug' ) continue;
                $meta_value = addslashes($meta_info->meta_value);
                $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
            }
            $sql_query.= implode(" UNION ALL ", $sql_query_sel);
            $wpdb->query($sql_query);
        }

        wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
        exit;
    } else {
        wp_die('Post creation failed, could not find original post: ' . $post_id);
    }
}
add_action( 'admin_action_bir_duplicate_post_as_draft', 'bir_duplicate_post_as_draft' );
 
function bir_duplicate_post_link( $actions, $post ) {
    if (current_user_can('edit_posts')) {
        $actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=bir_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Clone</a>';
    }
    return $actions;
}
add_filter( 'post_row_actions', 'bir_duplicate_post_link', 10, 2 ); // duplicate posts
add_filter('page_row_actions', 'bir_duplicate_post_link', 10, 2); // duplicate pages


# ------------------------------------------
# DISPLAY CPTs TO AT A GLANCE WIDGET
# ------------------------------------------
add_action( 'dashboard_glance_items', 'bir_dashboard_glance_cpt' );
function bir_dashboard_glance_cpt() {
    $args = array(
        'public' => true,
        '_builtin' => false
    );
    $output = 'object';
    $operator = 'and';
    $post_types = get_post_types( $args, $output, $operator );
    
    foreach ( $post_types as $post_type ) {
        $num_posts = wp_count_posts( $post_type->name );
        $num = number_format_i18n( $num_posts->publish );
        $text = _n( $post_type->labels->singular_name, $post_type->labels->name, intval( $num_posts->publish ) );
        if ( current_user_can( 'edit_posts' ) ) {
            $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
            echo '<li class="post-count ' . $post_type->name . '-count">' . $output . '</li>';
        } else {
            $output = '<span>' . $num . ' ' . $text . '</span>';
            echo '<li class="post-count ' . $post_type->name . '-count">' . $output . '</li>';
        }
    }
}


# ------------------------------------------
# ADMIN CSS // REMOVE FROM CUSTOMISER
# ------------------------------------------
add_action( 'customize_register', 'bir_customize_register' );
function bir_customize_register( $wp_customize ) {
	$wp_customize->remove_control( 'custom_css' );
}


# ------------------------------------------
# ADMIN PAGES // ADMIN BAR
# ------------------------------------------
function ir_remove_menus() {
    remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'ir_remove_menus' );

function remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
    $wp_admin_bar->remove_menu('comments');         // Remove the comments link
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );


# ------------------------------------------
# ADD ADMIN CSS
# ------------------------------------------
function bir_admin_style() {
    echo '<link rel="stylesheet" href="'.get_bloginfo('template_url').'/styles/admin/admin-style.css">';
}
add_action('admin_head', 'bir_admin_style');


# ------------------------------------------
# LOGIN SCREEN / ADMIN
# ------------------------------------------
function bir_login_logo() { ?>
    <style type="text/css">
		.login-action-login,
		.login-action-lostpassword,
		.login-action-rp {
			display: flex;
			flex-direction: row;
			flex-wrap: wrap;
			justify-content: center;
			align-items: center;
			background: #fff;
			position: relative;
		}
		.login-action-login:before,
		.login-action-lostpassword:before,
		.login-action-rp:before {
			content: "";
			display: block;
			width: 100%;
			height: 15px;
			background: #002c73;
			position: absolute;
			top: 0;
			left: 0;
		}
		
		.interim-login.login-action-login:before {
			display: none;
		}
		.interim-login #login {
			margin-top: 26px;
		}
		
		.login-action-login #login h1 a,
		.login-action-lostpassword #login h1 a,
		.login-action-rp #login h1 a {
		    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/inc/whitbread-logo.svg);
		    margin-bottom: 25px;
		    padding-bottom: 0;
		    width: 284px;
		    height: 34px;
		    background-size: 284px 34px;
		}
		.login-action-login #loginform,
		.login-action-lostpassword #lostpasswordform,
		.login-action-rp #resetpassform {
			margin: 0 0 18px 0;
			padding: 20px 20px 18px 20px;
			background: #002c73;
			background: -moz-linear-gradient(left, #014587 0%, #002c73 100%);
			background: -webkit-linear-gradient(left, #014587 0%,#002c73 100%);
			background: linear-gradient(to right, #014587 0%,#002c73 100%);
			border: none;
			box-shadow: none;
		}
		.login-action-login label,
		.login-action-lostpassword label,
		.login-action-rp label {
			padding-bottom: 3px;
			color: #fff;
		}
		.login-action-login #loginform input,
		.login-action-lostpassword #lostpasswordform input,
		.login-action-rp #resetpassform input {
			border: none;
			border-radius: 0;
			transition: all .2s ease-in-out;
		}
		.login-action-login #loginform input:active,
		.login-action-login #loginform input:focus,
		.login-action-rp #resetpassform input:focus,
		.login-action-lostpassword #lostpasswordform input:active,
		.login-action-lostpassword #lostpasswordform input:focus,
		.login-action-rp #resetpassform input:focus {
			border: 1px solid #adadad;
			outline: none!important;
			-webkit-box-shadow: none!important;
			box-shadow: none!important;
			border-radius: none!important;
		}
		.login .button.wp-hide-pw .dashicons {
			color: #002c73;
			transition: color .2s ease-in-out;
		}
		.login .button.wp-hide-pw .dashicons:hover,
		.login .button.wp-hide-pw .dashicons:active,
		.login .button.wp-hide-pw .dashicons:focus {
			color: #636362;
		}
		.login-action-login form .button-primary,
		.login-action-lostpassword form .button-primary,
		.login-action-rp form .button-primary {
			font-size: 12px!important;
			line-height: 2.1!important;
			text-transform: uppercase;
			color: #fff;
			background: transparent;
			border: 1px solid #fff!important;
			border-radius: 0!important;
		}
		.login-action-login form .button-primary:hover,
		.login-action-login form .button-primary:active,
		.login-action-login form .button-primary:focus,
		.login-action-rp form .button-primary:hover,
		.login-action-rp form .button-primary:active,
		.login-action-rp form .button-primary:focus {
			color: #002c73;
			background: #fff;
		}
		.login.wp-core-ui .button:active,
		.login.wp-core-ui .button:focus,
		.login.wp-core-ui .button-secondary:active,
		.login.wp-core-ui .button-secondary:focus {
			border: none!important;
			outline: none!important;
			-webkit-box-shadow: none!important;
			box-shadow: none!important;
			border-radius: none!important;
		}
		.login-action-login .privacy-policy-page-link,
		.login-action-rp .privacy-policy-page-link {
			display: none;
		}
		.login-action-login #login > p,
		.login-action-lostpassword #login > p,
		.login-action-rp #login > p {
			margin: 0 0 8px 0;
			padding: 0 20px;
		}
		.login-action-login #login > p a,
		.login-action-lostpassword #login > p a,
		.login-action-rp #login > p a {
			color: #202431;
			transition: color .2s ease-in-out;
		}
		.login-action-login #login > p a:hover,
		.login-action-login #login > p a:active,
		.login-action-login #login > p a:focus,
		.login-action-lostpassword #login > p a:hover,
		.login-action-lostpassword #login > p a:active,
		.login-action-lostpassword #login > p a:focus,
		.login-action-rp #login > p a:hover,
		.login-action-rp #login > p a:active,
		.login-action-rp #login > p a:focus {
			color: #545b71;
		}
		.login #login_error, 
		.login .message, 
		.login .success {
			padding: 14px 20px!important;
			margin-bottom: 20px!important;
			color: #202431;
			border: none!important;
		}
		
		#wp-auth-check-wrap #wp-auth-check {
			background-color: #fff!important;
		}
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'bir_login_logo' );

function bir_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'bir_login_logo_url' );

function bir_login_logo_url_title() {
    return 'site';
}
add_filter( 'login_headertext', 'bir_login_logo_url_title' );

function bir_footer_admin () {
    echo 'designed &amp; built by <a class="brand" href="https://www.brighterir.com">BRIGHTER<span>*</span><strong>IR</strong></a>';
}
add_filter('admin_footer_text', 'bir_footer_admin');



# ------------------------------------------
# password protected custom theme
# ------------------------------------------
function bir_password_protected_theme_file( $file ) {
    return get_stylesheet_directory() . '/plugins/branded-login/branded-login.php';
}
add_filter( 'password_protected_theme_file', 'bir_password_protected_theme_file' );


if (class_exists('ACF')) {

# ------------------------------------------
# ACF : PRO KEY
# ------------------------------------------
	function bir_acf_pro_key() {
	  acf_pro_update_license( 'b3JkZXJfaWQ9Mzc3MzZ8dHlwZT1wZXJzb25hbHxkYXRlPTIwMTQtMDgtMTkgMTk6MzQ6MTQ=' );
	}
	add_action('init', 'bir_acf_pro_key');

# ------------------------------------------
# ACF FILEDS
# ------------------------------------------
	add_filter('acf/settings/save_json', 'bir_acf_json_save_point');
	function bir_acf_json_save_point( $path ) {
	    $path = get_stylesheet_directory() . '/plugins/acf-json';
	    return $path;
	}
	add_filter('acf/settings/load_json', 'bir_acf_json_load_point');
	function bir_acf_json_load_point( $paths ) {
	    unset($paths[0]);
	    $paths[] = get_stylesheet_directory() . '/plugins/acf-json';
	    return $paths;
	}

}