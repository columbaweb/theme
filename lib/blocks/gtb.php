<?php 


# ------------------------------------------
# GUTENBERG : extend blocks
# ------------------------------------------
function cmb_gtb_enqueue() {
	wp_enqueue_script(
		'gtb-js',
		get_template_directory_uri() . '/assets/js/gtb.js',
		array( 'wp-blocks', 'wp-editor', 'wp-components', 'wp-i18n', 'wp-hooks' )
	);
}
add_action( 'enqueue_block_editor_assets', 'cmb_gtb_enqueue' );


# ------------------------------------------
# GUTENBERG : disable custom colours
# ------------------------------------------
function bir_gutenberg_disable_custom_colors() {
	add_theme_support( 'disable-custom-colors' );
}
add_action( 'after_setup_theme', 'bir_gutenberg_disable_custom_colors' );


# ------------------------------------------
# GUTENBERG : custom colour palette
# ------------------------------------------
function bir_gutenberg_color_palette() {
	add_theme_support(
		'editor-color-palette', array(
			array(
				'name'  => esc_html__( 'Storm Dust', '@@bir' ),
				'slug' => 'body',
				'color' => '#636362',
			),
			array(
				'name'  => esc_html__( 'Light Grey', '@@bir' ),
				'slug' => 'light-grey',
				'color' => '#fcfcfc',
			),
			array(
				'name'  => esc_html__( 'White', '@@bir' ),
				'slug' => 'white',
				'color' => '#fff',
			),
			array(
				'name'  => esc_html__( 'Catalina Blue', '@@bir' ),
				'slug' => 'catalina-blue',
				'color' => '#002c73',
			),
			array(
				'name'  => esc_html__( 'Congress Blue', '@@bir' ),
				'slug' => 'congress-blue',
				'color' => '#014587',
			),
			array(
				'name'  => esc_html__( 'Byzantium', '@@bir' ),
				'slug' => 'byzantium',
				'color' => '#6e266e',
			),
			array(
				'name'  => esc_html__( 'California', '@@bir' ),
				'slug' => 'california',
				'color' => '#f59d09',
			),	
			array(
				'name'  => esc_html__( 'Christi', '@@bir' ),
				'slug' => 'christi',
				'color' => '#7d9929',
			),
			array(
				'name'  => esc_html__( 'Red', '@@bir' ),
				'slug' => 'red',
				'color' => '#db092f',
			)		
		)
	);
}
add_action( 'after_setup_theme', 'bir_gutenberg_color_palette' );


# ------------------------------------------
# GUTENBERG : custom font sizes
# ------------------------------------------
add_action( 'after_setup_theme', 'ttp_custom_font_sizes' );

function ttp_custom_font_sizes() {
	add_theme_support( 'editor-font-sizes', array(
		array(
			'name'      => __( 'small', 'bir' ),
			'shortName' => __( 'S', 'bir' ),
			'size'      => 12,
			'slug'      => 'small'
		),
		array(
			'name'      => __( 'regular', 'bir' ),
			'shortName' => __( 'M', 'bir' ),
			'size'      => 16,
			'slug'      => 'regular'
		)
	) );
}

# ------------------------------------------
# GUTENBERG : wide and full image alignment
# ------------------------------------------ 
function writy_setup() {
	add_theme_support( 'align-wide' );
}
add_action( 'after_setup_theme', 'writy_setup' );


# ------------------------------------------
# REGISTER BIR BLOCK CATEGORY
# ------------------------------------------
function bir_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'bir',
				'title' => __( 'BIR', 'ir' ),
			),
		)
	);
}
add_filter( 'block_categories', 'bir_block_category', 1, 2);


# ------------------------------------------
# ACF: GET FILE
# ------------------------------------------
function get_acf_file( $key = 'file', $post_id = '' ) {
	if($post_id == ''):
		global $post;
		$post_id = $post->ID;
	endif;

	$file = get_field($key, $post_id);

	if( !empty($file) ):
		$file_url = $file['url'];
		$file_type = get_file_type($file['mime_type']);

		$file_size = get_file_size($file_url);
	endif;

	$file_details = array(
		'url'  => $file_url,
		'type' => $file_type,
		'size' => $file_size
	);

	return $file_details;
}

# ------------------------------------------
# ACF: GET FILE SIZE
# ------------------------------------------
function get_file_size( $file_url ) {

    $file_info = array_change_key_case( get_headers($file_url, TRUE) );
    $file_size = file_size_convert( $file_info['content-length'] );

    return $file_size;
}

# ------------------------------------------
# ACF: CONVERT FILE TYPE
# ------------------------------------------
function file_size_convert($bytes) {
    $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "," , strval(round($result, 0)))."".$arItem["UNIT"];
            break;
        }
    }
    return $result;
}

# ------------------------------------------
# ACF: GET FILE TYPE
# ------------------------------------------
function get_file_type( $mime_type ) {

    if( preg_match('/msword/', $mime_type) || preg_match('/officedocument\.wordprocessingml\.document/', $mime_type) ):
        $type = 'DOC';
    elseif(preg_match('/mp3/', $mime_type)):
        $type = 'MP3';
    elseif(preg_match('/pdf/', $mime_type)):
        $type = 'PDF';
    elseif(preg_match('/ms-powerpoint/', $mime_type) || preg_match('/officedocument\.presentationml/', $mime_type) ):
        $type = 'PPT';
    
    elseif(preg_match('/ms-excel/', $mime_type) ):
        $type = 'XLS';
    elseif(preg_match('/vnd.ms-excel/', $mime_type) ):
        $type = 'XLS';
    elseif(preg_match('/msexcel/', $mime_type) ):
        $type = 'XLS';
    elseif(preg_match('/x-msexcel/', $mime_type) ):
        $type = 'XLS';
    elseif(preg_match('/x-ms-excel/', $mime_type) ):
        $type = 'XLS';
    elseif(preg_match('/x-excel/', $mime_type) ):
        $type = 'XLS';
    elseif(preg_match('/x-dos_ms_excel/', $mime_type) ):
        $type = 'XLS';    
    elseif(preg_match('/xls/', $mime_type) ):
        $type = 'XLS'; 
    elseif(preg_match('/x-xls/', $mime_type) ):
        $type = 'XLS'; 
    elseif(preg_match('/vnd.openxmlformats-officedocument.spreadsheetml.sheet/', $mime_type) ):
        $type = 'XLS'; 
    endif;

    return $type;
}