<?php 


# ------------------------------------------
# BLOCK : button
# ------------------------------------------
function register_button_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'button',
		'title'			=> __( 'Button', 'bir' ),
		'render_template'	=> 'template-parts/blocks/button.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'preview',
		'keywords'		=> array( 'button' )
	));
}
add_action('acf/init', 'register_button_block_type' );


# ------------------------------------------
# BLOCK : link cta box
# ------------------------------------------
function register_cta_box_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'cta-bx',
		'title'			=> __( 'Link CTA Box', 'bir' ),
		'render_template'	=> 'template-parts/blocks/cta-box.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'welcome-view-site',
		),
		'supports' => array(
			'align' => true,
			'mode' => false,
			'jsx' => true
		),
		'keywords'		=> array( 'content', 'cta', 'link', 'box' )
	));
}
add_action('acf/init', 'register_cta_box_block_type' );


# ------------------------------------------
# BLOCK : bg cta box
# ------------------------------------------
function register_bg_cta_box_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'bg_cta-bx',
		'title'			=> __( 'Background CTA Box', 'bir' ),
		'render_template'	=> 'template-parts/blocks/bg-cta-box.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'welcome-view-site',
		),
		'supports' => array(
			'align' => true,
			'mode' => false,
			'jsx' => true
		),
		'keywords'		=> array( 'content', 'cta', 'background', 'box' )
	));
}
add_action('acf/init', 'register_bg_cta_box_block_type' );


# ------------------------------------------
# BLOCK : blue image box
# ------------------------------------------
function register_blue_cta_box_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'blue_cta',
		'title'			=> __( 'Blue CTA Box', 'bir' ),
		'render_template'	=> 'template-parts/blocks/blue-cta-box.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'welcome-view-site',
		),
		'supports' => array(
			'align' => true,
			'mode' => false,
			'jsx' => true
		),
		'keywords'		=> array( 'content', 'cta', 'background', 'box' )
	));
}
add_action('acf/init', 'register_blue_cta_box_block_type' );


# ------------------------------------------
# BLOCK : ICON LINKS
# ------------------------------------------
function register_icon_links_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'icon-links',
		'title'			=> __( 'Icon Links', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/icon-links.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'preview',
		'keywords'		=> array( 'button', 'link', 'icon' )
	));
}
add_action('acf/init', 'register_icon_links_block_type' );


# ------------------------------------------
# BLOCK : ICON LIST
# ------------------------------------------
function register_icon_list_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'icon-list',
		'title'			=> __( 'Icon List', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/icon-list.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'preview',
		'keywords'		=> array( 'list', 'icon' )
	));
}
add_action('acf/init', 'register_icon_list_block_type' );


# ------------------------------------------
# BLOCK : MARKETS
# ------------------------------------------
function register_market_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'market',
		'title'			=> __( 'Market', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/market.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'preview',
		'keywords'		=> array( 'market', 'map' )
	));
}
add_action('acf/init', 'register_market_block_type' );


# ------------------------------------------
# BLOCK : OPPORTUNITIES
# ------------------------------------------
function register_opportunities_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'opprtunity',
		'title'			=> __( 'Opportunity', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/opportunities.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'opportunity', 'toggle', 'accordion' )
	));
}
add_action('acf/init', 'register_opportunities_block_type' );


# ------------------------------------------
# GUTENBERG: UK/GERMAN MARKET CHART
# ------------------------------------------
function register_whitbread_chart_block_type() {
    if( ! function_exists( 'acf_register_block_type' ) )
        return;
    acf_register_block_type( array(
        'name'			=> 'bir-whitbread-chart',
        'title'			=> __( 'UK/German Market Chart', 'bir' ),
        'render_template'	=> 'template-parts/blocks/whitbread-chart.php',
        'category'		=> 'bir',
		'icon'			=> array( 'src' => 'chart-bar', 'foreground' => '#00c1bd' ),
        'mode'			=> 'auto',
        'keywords'		=> array( 'uk', 'german', 'market', 'chart' )
    ));
}
add_action('acf/init', 'register_whitbread_chart_block_type' );


# ------------------------------------------
# BLOCK : OPERATING MODEL
# ------------------------------------------
function register_model_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'model',
		'title'			=> __( 'Operating Model', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/model.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'preview',
		'keywords'		=> array( 'operations', 'model', 'chart' )
	));
}
add_action('acf/init', 'register_model_block_type' );


# ------------------------------------------
# BLOCK : PRIORITIES
# ------------------------------------------
function register_priotities_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'priorities',
		'title'			=> __( 'Priorities', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/prioities.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'priority', 'list', 'chart' )
	));
}
add_action('acf/init', 'register_priotities_block_type' );


# ------------------------------------------
# BLOCK : PRIORITIES
# ------------------------------------------
function register_chain_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'chain',
		'title'			=> __( 'Chain', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/chain.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'chain', 'chart' )
	));
}
add_action('acf/init', 'register_chain_block_type' );


# ------------------------------------------
# BLOCK : timeline
# ------------------------------------------
function register_timeline_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'timeline',
		'title'			=> __( 'Timeline', 'threethirty' ),
		'render_template'	=> 'template-parts/blocks/timeline.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'excerpt-view',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'timeline', 'history' )
	));
}
add_action('acf/init', 'register_timeline_block_type' );


# ------------------------------------------
# GUTENBERG : IFRAME
# ------------------------------------------
function register_iframe_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'iframe',
		'title'			=> __( 'Iframe', 'ir' ),
		'render_template'	=> 'template-parts/blocks/iframe.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'admin-tools',
		),
		'mode'			=> 'preview',
		'keywords'		=> array( 'iframe', 'tool' )
	));
}
add_action('acf/init', 'register_iframe_block_type' );


# ------------------------------------------
# GUTENBERG : COUNTER
# ------------------------------------------
function register_counter_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'counter',
		'title'			=> __( 'Counter', 'ir' ),
		'render_template'	=> 'template-parts/blocks/counter.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-ol-rtl',
		),
		'mode'			=> 'preview',
		'keywords'		=> array( 'counter', 'numbers', 'stat' )
	));
}
add_action('acf/init', 'register_counter_block_type' );


# ------------------------------------------
# BLOCK : video lightbox
# ------------------------------------------
function register_video_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'videopopup',
		'title'			=> __( 'Video Lightbox', 'bir' ),
		'render_template'	=> 'template-parts/blocks/video.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'video-alt3',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'video', 'popup', 'lightbox', 'youtube', 'vimeo', 'button' )
	));
}
add_action('acf/init', 'register_video_block_type' );


# ------------------------------------------
# BLOCK : video player
# ------------------------------------------
function register_video_player_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'video-payer',
		'title'			=> __( 'YouTube Player', 'bir' ),
		'render_template'	=> 'template-parts/blocks/yt-video.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'video-alt3',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'video', 'player', 'youtube' )
	));
}
add_action('acf/init', 'register_video_player_block_type' );


# ------------------------------------------
# BLOCK : latest posts
# ------------------------------------------
function register_latest_posts_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'latest-posts',
		'title'			=> __( 'Latest Posts', 'bir' ),
		'render_template'	=> 'template-parts/blocks/latest-posts.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'media-text',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'post', 'news', 'category', 'latest' )
	));
}
add_action('acf/init', 'register_latest_posts_block_type' );


# ------------------------------------------
# BLOCK : media
# ------------------------------------------
function register_media_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'media',
		'title'			=> __( 'Media', 'bir' ),
		'render_template'	=> 'template-parts/blocks/media.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'media-text',
		),
		'supports' => array(
			'align' => false,
			'mode' => false
		),
		'keywords'		=> array( 'post', 'news', 'media', 'filter', 'facet' )
	));
}
add_action('acf/init', 'register_media_block_type' );


# ------------------------------------------
# BLOCK : ADVISER
# ------------------------------------------
function register_adviser_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'adviser',
		'title'			=> __( 'Adviser', 'ir' ),
		'render_template'	=> 'template-parts/blocks/adviser.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'businessman',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'adviser', 'analyst', 'coverage' )
	));
}
add_action('acf/init', 'register_adviser_block_type' );


# ------------------------------------------
# BLOCK : CONTACT PERSON
# ------------------------------------------
function register_contact_person_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'contac-person',
		'title'			=> __( 'Contact Person', 'ir' ),
		'render_template'	=> 'template-parts/blocks/contact-person.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'businessman',
		),
		'mode'			=> 'preview',
		'keywords'		=> array( 'contact', 'person', 'info' )
	));
}
add_action('acf/init', 'register_contact_person_block_type' );


# ------------------------------------------
# BLOCK : team
# ------------------------------------------
if(true === get_theme_mod('bir_add_team')) {

	function register_team_block_type() {
		
		if( ! function_exists( 'acf_register_block_type' ) )
			return;
	
		acf_register_block_type( array(
			'name'			=> 'team',
			'title'			=> __( 'Team', 'bir' ),
			'render_template'=> 'template-parts/blocks/team.php',
			'category'		=> 'bir',
			'icon' => array(
				'foreground' => '#00c1bd',
				'src' => 'admin-users',
			),
			'mode'			=> 'auto',
			'keywords'		=> array('team', 'peope', 'profile')
		));
	}
	
	add_action('acf/init', 'register_team_block_type');
	
} // if team module selected


# ------------------------------------------
# BLOCK : careers
# ------------------------------------------
if(true === get_theme_mod('bir_add_jobs')) {

	function register_careers_block_type() {
		
		if( ! function_exists( 'acf_register_block_type' ) )
			return;
	
		acf_register_block_type( array(
			'name'			=> 'careers',
			'title'			=> __( 'Careers', 'bir' ),
			'render_template'	=> 'template-parts/blocks/careers.php',
			'category'		=> 'bir',
			'icon' => array(
				'foreground' => '#00c1bd',
				'src' => 'id',
			),
			'mode'			=> 'auto',
			'keywords'		=> array('career', 'job', 'vacancy')
		));
	}
	
	add_action('acf/init', 'register_careers_block_type');
	
} // if team module selected


# ------------------------------------------
# BLOCK : documents
# ------------------------------------------
if(true === get_theme_mod('bir_add_docs')) {

	function register_documents_block_type() {
		
		if( ! function_exists( 'acf_register_block_type' ) )
			return;
	
		acf_register_block_type( array(
			'name'			=> 'documents',
			'title'			=> __( 'Documents', 'bir' ),
			'render_template'	=> 'template-parts/blocks/documents.php',
			'category'		=> 'bir',
			'icon' => array(
				'foreground' => '#00c1bd',
				'src' => 'format-aside',
			),
			'mode'			=> 'auto',
			'keywords'		=> array('file', 'document')
		));
	}
	
	add_action('acf/init', 'register_documents_block_type');
	
	function register_latest_results_block_type() {
		
		if( ! function_exists( 'acf_register_block_type' ) )
			return;
	
		acf_register_block_type( array(
			'name'			=> 'latest-results',
			'title'			=> __( 'Latest Results', 'bir' ),
			'render_template'	=> 'template-parts/blocks/latest-results.php',
			'category'		=> 'bir',
			'icon' => array(
				'foreground' => '#00c1bd',
				'src' => 'format-aside',
			),
			'mode'			=> 'auto',
			'keywords'		=> array('file', 'document', 'result', 'download')
		));
	}
	
	add_action('acf/init', 'register_latest_results_block_type');
	
} // if team module selected


# ------------------------------------------
# BLOCK : events
# ------------------------------------------
if(true === get_theme_mod('bir_add_events')) {

# upcoming
	function register_events_block_type() {
		
		if( ! function_exists( 'acf_register_block_type' ) )
			return;
	
		acf_register_block_type( array(
			'name'			=> 'events-upcoming',
			'title'			=> __( 'Events - Upcoming', 'bir' ),
			'render_template'	=> 'template-parts/blocks/events-upcoming.php',
			'category'		=> 'bir',
			'icon' => array(
				'foreground' => '#00c1bd',
				'src' => 'calendar',
			),
			'mode'			=> 'auto',
			'keywords'		=> array('event', 'calendar')
		));
	}
	
	add_action('acf/init', 'register_events_block_type');

# past	
	function register_past_events_block_type() {
		
		if( ! function_exists( 'acf_register_block_type' ) )
			return;
	
		acf_register_block_type( array(
			'name'			=> 'events-past',
			'title'			=> __( 'Events - Past', 'bir' ),
			'render_template'	=> 'template-parts/blocks/events-past.php',
			'category'		=> 'bir',
			'icon' => array(
				'foreground' => '#00c1bd',
				'src' => 'calendar',
			),
			'mode'			=> 'auto',
			'keywords'		=> array('event', 'calendar')
		));
	}
	
	add_action('acf/init', 'register_past_events_block_type');
	
# latest
	function register_next_event_block_type() {
		
		if( ! function_exists( 'acf_register_block_type' ) )
			return;
	
		acf_register_block_type( array(
			'name'			=> 'events-next',
			'title'			=> __( 'Upcoming Event', 'bir' ),
			'render_template'	=> 'template-parts/blocks/events-next.php',
			'category'		=> 'bir',
			'icon' => array(
				'foreground' => '#00c1bd',
				'src' => 'calendar',
			),
			'mode'			=> 'auto',
			'keywords'		=> array('event', 'calendar')
		));
	}
	
	add_action('acf/init', 'register_next_event_block_type');

	
} // if team module selected


# ------------------------------------------
# BLOCK : CASE STUDIES CAROUSEL
# ------------------------------------------
function register_case_study_carousel_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'case-study-carousle',
		'title'			=> __( 'Case study carousel', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/case-study-carousel-new.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'carousel', 'case', 'study' )
	));
}
add_action('acf/init', 'register_case_study_carousel_block_type' );


# ------------------------------------------
# BLOCK : CASE STUDIES TOGGLE
# ------------------------------------------
function register_case_study_toggle_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'case-study-toggle',
		'title'			=> __( 'Case study grid', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/case-study-toggle.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'toggle', 'case', 'study', 'grid' )
	));
}
add_action('acf/init', 'register_case_study_toggle_block_type' );


# ------------------------------------------
# BLOCK : IMAGE CAROUSEL
# ------------------------------------------
function register_image_carousel_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'image-carousel',
		'title'			=> __( 'Image carousel', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/image-carousel.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'carousel' )
	));
}
add_action('acf/init', 'register_image_carousel_block_type' );


# ------------------------------------------
# BLOCK : BRANDS CAROUSEL
# ------------------------------------------
function register_brands_carousel_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'brands-carousel',
		'title'			=> __( 'Brands carousel', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/brands-carousel.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'carousel', 'case', 'study' )
	));
}
add_action('acf/init', 'register_brands_carousel_block_type' );


# ------------------------------------------
# BLOCK : BRANDS
# ------------------------------------------
function register_brands_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'brands',
		'title'			=> __( 'Brands', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/brands.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'carousel' )
	));
}
add_action('acf/init', 'register_brands_block_type' );


# ------------------------------------------
# BLOCK : BRAND TITLES
# ------------------------------------------
function register_brand_title_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'brand-title',
		'title'			=> __( 'Brand title', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/brand-title.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'editor-break',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'brand', 'title', 'logo', 'heading' )
	));
}
add_action('acf/init', 'register_brand_title_block_type' );


# ------------------------------------------
# BLOCK : SUSTAINABILITY INFOGRAPHIC
# ------------------------------------------
function register_sustainability_inforgtaphic_block_type() {
	
	if( ! function_exists( 'acf_register_block_type' ) )
		return;

	acf_register_block_type( array(
		'name'			=> 'sust-info',
		'title'			=> __( 'Sustainability infographic', 'cmb' ),
		'render_template'	=> 'template-parts/blocks/sust-info.php',
		'category'		=> 'bir',
		'icon' => array(
			'foreground' => '#00c1bd',
			'src' => 'randomize',
		),
		'mode'			=> 'auto',
		'keywords'		=> array( 'sustainability', 'infographic', 'chart', 'features' )
	));
}
add_action('acf/init', 'register_sustainability_inforgtaphic_block_type' );





