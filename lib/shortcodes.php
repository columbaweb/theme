<?php

# ------------------------------------------
# COUNTRY POPUP
# ------------------------------------------
function get_country_popup($atts) {
ob_start();
get_template_part( 'template-parts/content','popup' );
$output =  ob_get_clean();
return $output;
}
add_shortcode('country_popup', 'get_country_popup');


