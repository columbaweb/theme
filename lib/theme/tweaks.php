<?php

# ---------------------------------------------------
# Get unique ID
# ---------------------------------------------------
function bir_unique_id( $prefix = '' ) {
	static $id_counter = 0;
	if ( function_exists( 'wp_unique_id' ) ) {
		return wp_unique_id( $prefix );
	}
	return $prefix . (string) ++$id_counter;
}


# ---------------------------------------------------
# AUTO ALT TAG
# ---------------------------------------------------
add_action( 'add_attachment', 'bir_auto_alt' );
function bir_auto_alt( $post_ID ) {
	if ( wp_attachment_is_image( $post_ID ) ) {
		$my_image_title = get_post( $post_ID )->post_title;
		$my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',  $my_image_title );
		$my_image_title = ucwords( strtolower( $my_image_title ) );
		$my_image_meta = array(
			'ID'		=> $post_ID,			// Specify the image (ID) to be updated
			'post_title'	=> $my_image_title,		// Set image Title to sanitized title
			//'post_excerpt'	=> $my_image_title,		// Set image Caption (Excerpt) to sanitized title
			//'post_content'	=> $my_image_title,		// Set image Description (Content) to sanitized title
		);

		update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );
		wp_update_post( $my_image_meta );
	} 
}
