<?php


# ------------------------------------------
# CPT - CASE STUDIES
# ------------------------------------------
function post_type_case_study() {
    $labels = array(
        'name' => _x('Case study', 'post type general name'),
        'singular_name' => _x('Case study', 'post type singular name'),
        'menu_name' => _x('Case study', 'post type singular name'),
        'add_new' => _x('Add New', 'bir'),
        'add_new_item' => __('Add new case study'),
        'edit_item' => __('Edit case study'),
        'new_item' => __('New case study'),
        'view_item' => __('View case study'),
        'search_items' => __('Search case studies'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'menu_position'=> 5,
        'menu_icon' => 'dashicons-groups',
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'has_archive' => false,
        'show_in_rest' => true,
        'supports' => array('title', 'editor', 'thumbnail')
      );

    register_post_type( 'case-study' , $args );
}
add_action('init', 'post_type_case_study');

# CASE STUDY CATEGORIES
# ------------------------------------------
add_action( 'init', 'create_case_study_category', 0 );
function create_case_study_category() {
  $labels = array(
    'name' => _x( 'Case study category', 'taxonomy general name' ),
    'singular_name' => _x( 'Case study category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search categories' ),
    'all_items' => __( 'All categories' ),
    'parent_item' => __( 'Parent category' ),
    'parent_item_colon' => __( 'Parent category:' ),
    'edit_item' => __( 'Edit category' ),
    'update_item' => __( 'Update category' ),
    'add_new_item' => __( 'Add new category' ),
    'new_item_name' => __( 'New category name' ),
    'menu_name'         => __( 'Categories' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'show_in_rest' => true,
  );

  register_taxonomy( 'case-study-category', array( 'case-study' ), $args );
}


if(true === get_theme_mod('bir_add_team')) {
# ------------------------------------------
# CPT - TEAM
# ------------------------------------------
    function post_type_team() {
        $labels = array(
            'name' => _x('Team', 'post type general name'),
            'singular_name' => _x('Team', 'post type singular name'),
            'menu_name' => _x('Team', 'post type singular name'),
            'add_new' => _x('Add New', 'bir'),
            'add_new_item' => __('Add new person'),
            'edit_item' => __('Edit person'),
            'new_item' => __('New person'),
            'view_item' => __('View person'),
            'search_items' => __('Search team'),
            'not_found' =>  __('Nothing found'),
            'not_found_in_trash' => __('Nothing found in trash'),
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'menu_position'=> 20,
            'menu_icon' => 'dashicons-groups',
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'has_archive' => false,
            'supports' => array('title')
          );

        register_post_type( 'team' , $args );
    }
    add_action('init', 'post_type_team');

# TEAM CATEGORIES
# ------------------------------------------
    add_action( 'init', 'create_team_category', 0 );
    function create_team_category() {
      $labels = array(
        'name' => _x( 'Team category', 'taxonomy general name' ),
        'singular_name' => _x( 'Team category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search categories' ),
        'all_items' => __( 'All categories' ),
        'parent_item' => __( 'Parent category' ),
        'parent_item_colon' => __( 'Parent category:' ),
        'edit_item' => __( 'Edit category' ),
        'update_item' => __( 'Update category' ),
        'add_new_item' => __( 'Add new category' ),
        'new_item_name' => __( 'New category name' ),
        'menu_name'         => __( 'Categories' ),
      );

      $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
      );

      register_taxonomy( 'team-category', array( 'team' ), $args );
    }

# CHANGE 'Add title'
# ------------------------------------------
	function team_title_text_input ( $title ) {
	   if ( get_post_type() == 'team' ) {
	      $title = __( 'Name Surname' );
	   }
	   return $title;
	}
	add_filter( 'enter_title_here', 'team_title_text_input' );
	

# REDIRECT TO TEAM PAGE
# ------------------------------------------
	add_action( 'template_redirect', 'team_redirect_post' );
	function team_redirect_post() {
		$queried_post_type = get_query_var('post_type');
			if ( is_single() && 'team' ==  $queried_post_type ) {
				wp_redirect( get_bloginfo('url').'/about-us/management/', 301 );
			exit;
		}
	}	
    
} // if team module selected


if(true === get_theme_mod('bir_add_jobs')) {
# ------------------------------------------
# CPT - CAREERS
# ------------------------------------------
    function post_type_careers() {
        $labels = array(
            'name' => _x('Careers', 'post type general name'),
            'singular_name' => _x('Vacancy', 'post type singular name'),
            'menu_name' => _x('Vacancies', 'post type singular name'),
            'add_new' => _x('Add New', 'bir'),
            'add_new_item' => __('Add new vacancy'),
            'edit_item' => __('Edit vacancy'),
            'new_item' => __('New vacancy'),
            'view_item' => __('View vacancy'),
            'search_items' => __('Search vacancies'),
            'not_found' =>  __('Nothing found'),
            'not_found_in_trash' => __('Nothing found in trash'),
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'menu_position'=> 20,
            'menu_icon' => 'dashicons-id',
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'has_archive' => false,
            'supports' => array('title', 'editor')
          );

        register_post_type( 'careers' , $args );
    }
    add_action('init', 'post_type_careers');
    
# CHANGE 'Add title'
# ------------------------------------------
	function careers_title_text_input ( $title ) {
	   if ( get_post_type() == 'careers' ) {
	      $title = __( 'Job title' );
	   }
	   return $title;
	}
	add_filter( 'enter_title_here', 'careers_title_text_input' );    
	    
} // if careers module selected


if(true === get_theme_mod('bir_add_docs')) {
# ------------------------------------------
# CPT - DOCUMENTS
# ------------------------------------------
    function post_type_document() {
        $labels = array(
            'name' => _x('Documents', 'post type general name'),
            'singular_name' => _x('Document', 'post type singular name'),
            'menu_name' => _x('Documents', 'post type singular name'),
            'add_new' => _x('Add new', 'bir'),
            'add_new_item' => __('Add new document'),
            'edit_item' => __('Edit document'),
            'new_item' => __('New document'),
            'view_item' => __('View document'),
            'search_items' => __('Search documents'),
            'not_found' =>  __('Nothing found'),
            'not_found_in_trash' => __('Nothing found in trash'),
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'menu_position'=> 10,
            'menu_icon' => 'dashicons-welcome-write-blog',
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'has_archive' => false,
            'supports' => array('title',)
          );

        register_post_type( 'documents' , $args );
    }
    add_action('init', 'post_type_document');

# DOCUMENT CATEGORIES
# ------------------------------------------
    add_action( 'init', 'create_document_category', 0 );
    function create_document_category() {
      $labels = array(
        'name' => _x( 'Document type', 'taxonomy general name' ),
        'singular_name' => _x( 'Document type', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search types' ),
        'all_items' => __( 'All types' ),
        'parent_item' => __( 'Parent type' ),
        'parent_item_colon' => __( 'Parent type:' ),
        'edit_item' => __( 'Edit type' ),
        'update_item' => __( 'Update type' ),
        'add_new_item' => __( 'Add new type' ),
        'new_item_name' => __( 'New type name' ),
        'menu_name'         => __( 'Types' ),
      );

      $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
      );

      register_taxonomy( 'document-type', array( 'documents' ), $args );
    }
    
# CHANGE 'Add title'
# ------------------------------------------
	function documents_title_text_input ( $title ) {
	   if ( get_post_type() == 'documents' ) {
	      $title = __( 'Document name' );
	   }
	   return $title;
	}
	add_filter( 'enter_title_here', 'documents_title_text_input' );        
} // if docs module selected


if(true === get_theme_mod('bir_add_events')) {
# ------------------------------------------
# CPT - EVENTS
# ------------------------------------------
    function post_type_events() {
        $labels = array(
            'name' => _x('Events', 'post type general name'),
            'singular_name' => _x('Event', 'post type singular name'),
            'menu_name' => _x('Events', 'post type singular name'),
            'add_new' => _x('Add new', 'bir'),
            'add_new_item' => __('Add new event'),
            'edit_item' => __('Edit event'),
            'new_item' => __('New event'),
            'view_item' => __('View event'),
            'search_items' => __('Search event'),
            'not_found' =>  __('Nothing found'),
            'not_found_in_trash' => __('Nothing found in trash'),
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => false,
            'exclude_from_search' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_nav_menus' => false,
            'show_in_admin_bar' => false,
            'menu_position'=> 20,
            'menu_icon' => 'dashicons-calendar',
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'has_archive' => false,
            'supports' => array('title', 'editor')
          );

        register_post_type( 'events' , $args );
    }
    add_action('init', 'post_type_events');
    
# CHANGE 'Add title'
# ------------------------------------------
	function events_title_text_input ( $title ) {
	   if ( get_post_type() == 'events' ) {
	      $title = __( 'Event title' );
	   }
	   return $title;
	}
	add_filter( 'enter_title_here', 'events_title_text_input' );      
} // if calendar module selected



# ------------------------------------------
# CPT - DOCUMENTS
# ------------------------------------------
function post_type_brands() {
    $labels = array(
        'name' => _x('Brands', 'post type general name'),
        'singular_name' => _x('Brand', 'post type singular name'),
        'menu_name' => _x('Brands', 'post type singular name'),
        'add_new' => _x('Add new', 'bir'),
        'add_new_item' => __('Add new company'),
        'edit_item' => __('Edit company'),
        'new_item' => __('New company'),
        'view_item' => __('View company'),
        'search_items' => __('Search company'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'menu_position'=> 30,
        'menu_icon' => 'dashicons-palmtree',
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'has_archive' => false,
        'supports' => array('title')
      );

    register_post_type( 'brand' , $args );
}
add_action('init', 'post_type_brands');

# CHANGE 'Add title'
# ------------------------------------------
function brands_title_text_input ( $title ) {
   if ( get_post_type() == 'brand' ) {
      $title = __( 'Brand name' );
   }
   return $title;
}
add_filter( 'enter_title_here', 'brands_title_text_input' );