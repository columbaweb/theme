<?php 

# ------------------------------------------
# Populate year field for documents
# ------------------------------------------
add_action( 'save_post', 'bir_save_postdata' );
function bir_save_postdata( $post_id ) {
	$mydata = get_the_time('Y'); 
	update_field( 'post_year', $mydata, $post_id );
}


// hide counts --------------------------------------------------------------
add_filter( 'facetwp_facet_dropdown_show_counts', '__return_false' );
add_filter( 'facetwp_facet_radio_show_counts', '__return_false' );


// sort order --------------------------------------------------------------
add_filter( 'facetwp_facet_orderby', function( $orderby, $facet ) {
    if ( 'years' == $facet['name'] ) {
        $orderby = 'f.facet_value+0 DESC';
    }
    
    if ( 'categories' == $facet['name'] ) {
        $orderby = 'FIELD(f.facet_display_value, "Brand", "Estate", "Financial &amp; business", "Sustainability")';
    }

    return $orderby;
}, 10, 2 );