<?php

# ------------------------------------------
# LOAD TEXTDOMAIN
# ------------------------------------------
load_theme_textdomain( 'bir' );


# ------------------------------------------
# ADD SCRIPTS
# ------------------------------------------
function bir_scripts() {
	$style_ver = date('dmHiYs');
	
// default styles	
	wp_enqueue_style( 'sec-font', '//fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800;900&display=swap', false );
	wp_enqueue_style( 'style', get_template_directory_uri() . '/styles/style.css', array(), $style_ver );

// default scripts	
	wp_enqueue_script( 'bir-slider', get_template_directory_uri() . '/assets/js/slick.js', array('jquery'), '', true );
	wp_enqueue_script( 'countto', get_template_directory_uri() . '/assets/js/countto.js', array(), '', true );
	wp_enqueue_script( 'waypoint', get_template_directory_uri() . '/assets/js/jquery.waypoints.min.js', array(), '', true );
	wp_enqueue_script( 'bir-rwd-tables', get_template_directory_uri() . '/assets/js/stacktable.js', array(), '20151215', true );

	wp_enqueue_script( 'bir-custom-js', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), '', true );

// optional : icon fonts	
	wp_enqueue_style( 'dashicons' );
//	wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css' );

// optional : fancybox	
	wp_enqueue_style( 'fancybox', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css' );
	wp_enqueue_script( 'fancybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', array(), '', true );		

// comments
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bir_scripts' );

// script tags
add_filter( 'script_loader_tag', 'bir_script_integrity', 10, 3 );
function bir_script_integrity( $tag, $handle, $src ) {
    if ( 'fancybox-js' === $handle ) {
        $tag = '<script src="' . esc_url( $src ) . '" integrity="sha256-yt2kYMy0w8AbtF89WXb2P1rfjcP/HTHLT7097U8Y5b8=" crossorigin="anonymous"></script>';
    }
    return $tag;
}

# ------------------------------------------
# ADD THE SUPPORT :: MENUS FEATURED IMAGES
# ------------------------------------------
if (function_exists( 'add_theme_support' )) {
	add_theme_support('post-thumbnails', array( 'post', 'case-study') );
	add_theme_support('automatic-feed-links');
	add_theme_support( 'title-tag');
	add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption',));
	add_theme_support( 'post-formats', array('gallery', 'image', 'video') );
	add_theme_support('menus');

// Custom logo.
	$logo_width  = 120;
	$logo_height = 90;

// If the retina setting is active, double the recommended width and height.
	if ( get_theme_mod( 'retina_logo', false ) ) {
		$logo_width  = floor( $logo_width * 2 );
		$logo_height = floor( $logo_height * 2 );
	}

	add_theme_support(
		'custom-logo',
		array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-height' => true,
			'flex-width'  => true,
		)
	);

	set_post_thumbnail_size( 1280, 9999 );
	
	add_image_size( 'team', 580, 580, true);
	add_image_size( 'case', 450, 450, true);
	add_image_size( 'carousel', 960, 500, true);

	register_nav_menus(
        array(
            'primary' => __( 'Top menu' ),
            'footnav' => __( 'Legal menu' )
        )
    );
}


# CREATE DYNAMIC SIDEBARS FOR MEGAMENU
# ------------------------------------------
function bir_megamenus() {
    $location = 'primary';
    $css_class = 'has-mega-menu';
    $locations = get_nav_menu_locations();

    if ( isset( $locations[ $location ] ) ) {
        $menu = get_term( $locations[ $location ], 'nav_menu' );

        if ( $items = wp_get_nav_menu_items( $menu->name ) ) {
            foreach ( $items as $item ) {
                if ( in_array( $css_class, $item->classes ) ) {
                    register_sidebar( array(
                        'id'   => 'mega-menu-widget-area-' . $item->ID,
                        'name' => 'Mega Menu: '. $item->title,
                    ) );
                } elseif ( in_array( $css_class_ru, $item->classes ) ) {
                    register_sidebar( array(
                        'id'   => 'mega-menu-widget-area-' . $item->ID,
                        'name' => 'Mega Menu: '. $item->title,
                    ) );
                }
            }
        }
    }
}
add_action( 'widgets_init', 'bir_megamenus' );


# ------------------------------------------
# REGISTER SIDEBARS
# ------------------------------------------
function create_sidebar( $name, $id, $description, $class ) {
    register_sidebar(array(
        'name' => __( $name ),
        'id' => $id,
        'description' => __( $description ),
        'before_widget' => '<div id="%1$s" class="'. $class .' %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
}
//create_sidebar( 'Site sidebar', 'sidebar', 'Main site sidebar', 'widget' );
create_sidebar( 'Sidebar: Sustainability', 'sustainability', 'sustainability', 'widget' );
create_sidebar( 'Sidebar: Sustainability Landing', 'sustainability-landing', 'sustainability', 'widget' );
create_sidebar( 'Sidebar: Investors', 'investors', 'investors', 'widget' );
create_sidebar( 'Sidebar: Media', 'media', 'media', 'widget' );
create_sidebar( 'Sidebar: Property', 'property', 'property', 'widget' );
create_sidebar( 'Sidebar: Legal', 'legal', 'legal', 'widget' );
create_sidebar( 'Sidebar: Reports', 'reports', 'reports', 'widget' );
create_sidebar( 'Footer sidebar 1', 'footer-1', '', 'site-widget' );
create_sidebar( 'Footer sidebar 2', 'footer-2', '', 'site-widget' );
create_sidebar( 'Footer sidebar 3', 'footer-3', '', 'site-widget' );


# ------------------------------------------
# EXCERPT LENGTH
# ------------------------------------------
function bir_custom_excerpt_length( $length ) {
    return 50;
}
add_filter( 'excerpt_length', 'bir_custom_excerpt_length', 999 );


# ------------------------------------------
# CUSTOM EXCERPT LENGTH  -  p h p   e c h o   e x c e r p t ( 4 5 )
# ------------------------------------------
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ", $excerpt).'';
	} else {
		$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}


# ------------------------------------------
# CHECK FOR SUBPAGES
# ------------------------------------------
function is_child($pageID) {
    global $post;
    
    if( is_page() && ($post->post_parent==$pageID) ) {
        return true;
    } else {
        return false;
    }
}


# ---------------------------------------------------
# CHECK IF HAS CHILD PAGES
# ---------------------------------------------------
function has_children() {
  global $post;
  
  $pages = get_pages('child_of=' . $post->ID);
  
  if (count($pages) > 0):
    return true;
  else:
    return false;
  endif;
}


