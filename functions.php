<?php

# ------------------------------------------
# Theme customiser // admin
# ------------------------------------------
require get_template_directory() . '/lib/admin/backend.php';
require get_template_directory() . '/lib/admin/customiser.php';

# ------------------------------------------
# Theme setup
# ------------------------------------------
require get_template_directory() . '/lib/theme/setup.php';

# ------------------------------------------
# Theme tweaks
# ------------------------------------------
require get_template_directory() . '/lib/theme/tweaks.php';

# ------------------------------------------
# Gutenberg blocks
# ------------------------------------------
require get_template_directory() . '/lib/blocks/gtb.php';
require get_template_directory() . '/lib/blocks/gtb-blocks.php';

# ------------------------------------------
# Custom post types // meta
# ------------------------------------------
require get_template_directory() . '/lib/theme/cpt.php';

# ------------------------------------------
# ICAL ecport for the calendar
# ------------------------------------------
if(true === get_theme_mod('bir_add_events')) {
	require get_template_directory() . '/lib/ics.php';
}

# ------------------------------------------
# Facets
# ------------------------------------------
require get_template_directory() . '/lib/theme/facets.php';


# ------------------------------------------
# Shortcodes
# ------------------------------------------
require get_template_directory() . '/lib/shortcodes.php';


# ------------------------------------------
# Widgets
# ------------------------------------------
#require get_template_directory() . '/lib/widgets.php';