<?php get_header(); ?>

	<div class="wrap <?php echo get_theme_mod('bir_archive_sidebar'); ?>">
		
		<article id="content">
			
			<?php  if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>
				
				<h2><?php the_title(); ?></h2>
				
				<?php 
					get_template_part( 'template-parts/content', 'postmeta' ); 
					
					the_post_thumbnail();
					
					the_content();
					
					the_post_navigation(
						$args = array(
							'prev_text' => __('<span>previous</span> <strong>%title</strong>', 'ir'),
							'next_text' => __('<span>next</span> <strong>%title</strong>', 'ir'),
							'in_same_term' => true,
							'taxonomy'  => __( 'category' ),
						)
					);
				?>
				
			<?php } } ?>
			
		</article>
		
		<?php if( ((get_theme_mod('bir_archive_sidebar')) != "full-width") ) { ?>
		<aside id="sidebar" class="widget-area">
			<h2 class="side-title is-style-border"><a href="<?php bloginfo('url'); ?>/media/">Media</a></h2>
			
			<?php 
				wp_nav_menu( array(
				    'menu'  => 'Press release',
				) );
				
				dynamic_sidebar('media'); 
			?>
			
		</aside>
		<?php } ?>
	</div>

<?php get_footer(); ?>