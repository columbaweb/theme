<?php get_header(); ?>

	<div class="wrap full-width">
		<article id="content">
			<h1><?php esc_html_e( 'Error 404', 'bir' ); ?></h1>
			<p><?php esc_html_e( 'The page you are looking for does not exist.', 'bir' ); ?></p>
		</article>
	</div>

<?php get_footer(); ?>